//
//  InventoryDetailViewController.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/28/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

class InventoryDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Class methods
    func configureView() {
        UtilView.getImageForBackground(name: Constant_Key.invBg, view: self.view)
        UtilView.configureBackButton(vc: self, selector: #selector(self.goBack))
        self.navigationController?.presentTransparentNavigationBar()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: Constants.idDetailTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.idDetailTableViewCell)
        self.tableView.separatorInset = UIEdgeInsets.zero
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        self.tableView.separatorColor = UIColor.clear
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: Table View Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.idDetailTableViewCell, for: indexPath) as! DetailTableViewCell
        cell.titleLabel.text = "PEDRO"
        cell.detailLabel.text = "qwerasdfthrte"
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }

}
