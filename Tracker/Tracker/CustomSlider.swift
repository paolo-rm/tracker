//
//  CustomSlider.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/23/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

class CustomSlider: UISlider {

    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        var newBounds = super.trackRect(forBounds: bounds)
        newBounds.size.height = 5.0
        return newBounds
    }

}
