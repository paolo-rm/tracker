//
//  DetailTableViewCell.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/29/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

class DetailTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.backgroundColor = UIColor.clear
                UtilView.getImageForBackground(name: Constant_Key.invCellViewBg, view: self.containerView)
                self.titleLabel.backgroundColor = UIColor.clear
                self.detailLabel.backgroundColor = UIColor.clear
                self.tintColor = UIColor.white
                self.preservesSuperviewLayoutMargins = false
                self.separatorInset = UIEdgeInsets.zero
                self.layoutMargins = UIEdgeInsets.zero
            }
        }
    }
    
}
