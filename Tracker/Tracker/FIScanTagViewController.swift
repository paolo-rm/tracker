//
//  FIScanTagViewController.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/27/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit
import SwiftyJSON

class FIScanTagViewController: UIViewController, SearchBarListViewDelegate, TSLCommandConfigureDelegate {

    var searchBarListView: SearchBarListView!
    var readerCommand: TSLCommandConfigure!
    var tagSetter = NSMutableSet()
    var arrayString = [String]()
    var checkControl = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.configureView()
        self.readerCommand = TSLCommandConfigure(isMultipleScan: true)
        self.readerCommand.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Class methods
    func configureView() {
        UtilView.getImageForBackground(name: Constant_Key.invBg, view: self.view)
        UtilView.configureBackButton(vc: self, selector: #selector(self.goBack))
        self.navigationController?.presentTransparentNavigationBar()
        self.searchBarListView = SearchBarListView(frame: UtilView.getContainerFrame(viewController: self, notificationHeight: 60.0))
        self.view.addSubview(self.searchBarListView)
        self.searchBarListView.delegate = self
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func didSelectRowAtIndexPathInTableView(stringData: String, dictionaryData: JSON, isJSONData: Bool) {
        if let viewController = self.navigationController!.viewControllers[1] as? FIScanViewController {
            viewController.scanTagString = stringData
            if stringData.isEmpty {
                viewController.scanTagString = viewController.scanIt
            }
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func TSLTransponderReceived(epc: String!, crc: NSNumber!, pc: NSNumber!, rssi: NSNumber!, fastId: Data!, moreAvailable: Bool) {
        if !epc.isEmpty {
            self.insertTagEPC(epc: epc, moreAvailable: moreAvailable)
        }
    }
    
    func TSLBarcodeReceived(data: String) {
        if !data.isEmpty {
            self.insertTagEPC(epc: data, moreAvailable: false)
        }
    }
    
    func insertTagEPC(epc: String, moreAvailable: Bool) {
        if !self.tagSetter.contains(epc) {
            self.tagSetter.add(epc)
            self.arrayString.append(epc)
            if !moreAvailable {
                self.searchBarListView.reloadSearchBarList(stringArrayData: self.arrayString, checkControl: self.checkControl)
            }
        }
    }
    
}
