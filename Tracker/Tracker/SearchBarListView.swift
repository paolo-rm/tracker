//
//  SearchBarList.swift
//  vizix-ios-project
//
//  Created by Paolo Ramos on 7/18/16.
//  Copyright © 2016 Mojix. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol SearchBarListViewDelegate: class {
    func didSelectRowAtIndexPathInTableView(stringData: String, dictionaryData: JSON, isJSONData: Bool)
}

class SearchBarListView: UIView, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
    
    let nibName = "SearchBarListView"
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    weak var delegate: SearchBarListViewDelegate?
    var dataArray = [String]()
    var filterArray = [String]()
    var dictionaryKey = String()
    var JSONdataArray = [JSON]()
    var JSONfilterArray = [JSON]()
    var JSONkeyFilter = String()
    var isJSONdataControl = false
    var searchActive = false
    var checkControl = String()
    var sectionTitle = String()
    
    // MARK: View Methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.xibSetup()
    }
    
    func xibSetup() {
        self.view = loadViewFromNib()
        self.view.frame = bounds
        self.view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        self.addSubview(self.view)
        self.configureView()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func configureView() {
        self.searchBar.delegate = self
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.separatorInset = UIEdgeInsets.zero
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        self.tableView.separatorColor = UIColor.clear
        self.checkControl.removeAll()
    }
    
    // MARK: Class Methods
    func reloadSearchBarList(JSONKeyFilterData: String, JSONData: [JSON], checkControl: String) {
        self.checkControl = checkControl
        self.JSONkeyFilter = JSONKeyFilterData
        self.JSONdataArray = JSONData
        self.isJSONdataControl = !self.JSONkeyFilter.isEmpty
        self.tableView.reloadData()
    }
    
    func reloadSearchBarList(stringArrayData: [String], checkControl: String) {
        self.checkControl = checkControl
        self.JSONkeyFilter.removeAll()
        self.dataArray = stringArrayData
        self.isJSONdataControl = false
        self.tableView.reloadData()
    }
    
    // MARK: Table View Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            if self.isJSONdataControl{
                if self.searchActive{
                    return self.JSONfilterArray.count
                }
                return self.JSONdataArray.count
            }
            else{
                if self.searchActive{
                    return self.filterArray.count
                }
                return self.dataArray.count
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        cell.textLabel?.backgroundColor = UIColor.clear
        cell.textLabel?.textColor = UIColor.white
        
        // Separator View
        let separator = UIImageView(frame: CGRect(x: 80.0, y: cell.frame.height - 2.0, width: cell.frame.width - 80.0, height: 1.0))
        separator.backgroundColor = UIColor.clear
        separator.image = UIImage(named: Constant_Key.lineGradient)
        separator.contentMode = .scaleToFill
        cell.addSubview(separator)
        
        if indexPath.section == 0 {
            cell.textLabel?.text = "None"
            cell.accessoryType = .none
            if self.checkControl.isEmpty {
                cell.accessoryType = .checkmark
            }
            return cell
        }
        
        if indexPath.section == 1 {
            if self.isJSONdataControl{
                if self.searchActive{
                    cell.textLabel?.text = self.JSONfilterArray[indexPath.row][self.JSONkeyFilter].stringValue
                }
                else{
                    cell.textLabel?.text = self.JSONdataArray[indexPath.row][self.JSONkeyFilter].stringValue
                }
            }
            else{
                if self.searchActive{
                    cell.textLabel?.text = self.filterArray[indexPath.row]
                }
                else{
                    cell.textLabel?.text = self.dataArray[indexPath.row]
                }
            }
            cell.accessoryType = .none
            if !self.checkControl.isEmpty && cell.textLabel?.text == self.checkControl{
                cell.accessoryType = .checkmark
            }
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            self.delegate?.didSelectRowAtIndexPathInTableView(stringData: InventoryView.None, dictionaryData: JSON(NSData()), isJSONData: false)
        }
        else {
            self.searchBar.resignFirstResponder()
            if self.isJSONdataControl {
                let data: JSON
                if self.searchActive {
                    data = self.JSONfilterArray[indexPath.row]
                }
                else {
                    data = self.JSONdataArray[indexPath.row]
                }
                self.delegate?.didSelectRowAtIndexPathInTableView(stringData: String(), dictionaryData: data, isJSONData: self.isJSONdataControl)
            }
            else {
                let data: String
                if self.searchActive {
                    data = self.filterArray[indexPath.row]
                }
                else{
                    data = self.dataArray[indexPath.row]
                }
                self.delegate?.didSelectRowAtIndexPathInTableView(stringData: data, dictionaryData: JSON(NSDictionary()), isJSONData: self.isJSONdataControl)
            }
        }
    }
    
    // MARK: Search Bar Methods
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.searchBar.resignFirstResponder()
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if self.isJSONdataControl{
            self.JSONfilterArray = self.JSONdataArray.filter({ (item) -> Bool in
                let searchItem = item[self.JSONkeyFilter].stringValue
                let range =  NSString(string: searchItem).range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                return range.location != NSNotFound
            })
            self.searchActive = !searchText.isEmpty
        }
        else{
            self.filterArray = self.dataArray.filter({ (item) -> Bool in
                let range =  NSString(string: item).range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                return range.location != NSNotFound
            })
            self.searchActive = !searchText.isEmpty
        }
        self.tableView.reloadData()
    }
}
