//
//  TSLReaderConfiguration.swift
//  Tracker
//
//  Created by Paolo Ramos on 5/24/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

class TSLReaderConfiguration: NSObject {
    
    class func setFactoryDefault() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let commander = appDelegate.commander {
            if commander.isConnected {
                if let resetCommand = TSLFactoryDefaultsCommand.synchronousCommand() {
                    commander.execute(resetCommand)
                    if resetCommand.isSuccessful {
                        appDelegate.commander = commander
                        TSLReaderConfiguration.setRFIDReaderValues(TSLCommander: appDelegate.commander!)
                    }
                }
            }
        }
    }
    
    class func setRFIDReaderValues(TSLCommander: TSLAsciiCommander) {
        if TSLCommander.isConnected {
//            setPowerLevelReader(readerValue: TSLCommander)
            setVibrateReader()
            setSoundReader()
//            setAutoConnectEnabled()
        }
    }
    
    class func disconnect() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let commander = appDelegate.commander {
            if commander.isConnected {
                let sleepCommand: TSLSleepCommand = TSLSleepCommand()
                sleepCommand.responseDidFinish()
                commander.execute(sleepCommand)
                commander.disconnect()
            }
        }
    }
    
    class func resetTSLReader() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let commander = appDelegate.commander {
            if let resetCommand = TSLFactoryDefaultsCommand.synchronousCommand() {
                commander.execute(resetCommand)
                if resetCommand.isSuccessful {
                    appDelegate.commander = commander
                    TSLReaderConfiguration.setFactoryDefault()
                }
            }
        }
    }
    
    class func setAutoConnectEnabled() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let commander = appDelegate.commander {
            if commander.isConnected {
                var commandRSSI = String()
                if TSLLocalConfig.getReaderAutoConnect() {
                    commandRSSI = ".sl -ar on -cm iap -n"
                }
                else {
                    commandRSSI = ".sl -ar off -cm off -n"
                }
                commander.send(commandRSSI)
            }
        }
    }
    
    class func getQValue(scanMode:String) -> String {
        switch scanMode {
        case "multiScan":
            return ReaderPreferences.sharedInstance.getQMultiScan()
        case "singleScan":
            return ReaderPreferences.sharedInstance.getQSingleScan()
        case "geigerScan":
            return ReaderPreferences.sharedInstance.getQGeigerScan()
        default:
            return ReaderPreferences.sharedInstance.getQGeigerScan()
        }
    }
    
    class func getTargetValue(scanMode:String) -> String {
        switch scanMode {
        case "multiScan":
            return ReaderPreferences.sharedInstance.getTargetMultiScan()
        case "singleScan":
            return ReaderPreferences.sharedInstance.getTargetSingleScan()
        case "geigerScan":
            return ReaderPreferences.sharedInstance.getTargetGeigerScan()
        default:
            return ReaderPreferences.sharedInstance.getTargetSingleScan()
        }
    }
    
    class func getSessionValue(scanMode:String) -> String {
        switch scanMode {
        case "multiScan":
            return ReaderPreferences.sharedInstance.getSessionMultiScan()
        case "singleScan":
            return ReaderPreferences.sharedInstance.getSessionSingleScan()
        case "geigerScan":
            return ReaderPreferences.sharedInstance.getSessionGeigerScan()
        default:
            return ReaderPreferences.sharedInstance.getSessionSingleScan()
        }
    }
    
    class func getSelectValue(scanMode:String) -> String {
        switch scanMode {
        case "multiScan":
            return ReaderPreferences.sharedInstance.getSelectMultiScan()
        case "singleScan":
            return ReaderPreferences.sharedInstance.getSelectSingleScan()
        case "geigerScan":
            return ReaderPreferences.sharedInstance.getSelectGeigerScan()
        default:
            return ReaderPreferences.sharedInstance.getSelectSingleScan()
        }
    }
    
    class func setRFIDMultipleScanValues() {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        var commandRSSI = ".iv -al on -c off -dt off -e off -fi off -fs off -ie on -io on -ix off -n -o 29 -p -qa fix "
        if let commander = appDelegate.commander {
            if commander.isConnected {
                switch TSLReaderConfiguration.getSelectValue(scanMode: "multiScan") {
                case "No":
                    commandRSSI = commandRSSI.appending("-ql nsl ")
                    break
                case "Yes":
                    commandRSSI = commandRSSI.appending("-ql all ")
                    break
                default:
                    commandRSSI = commandRSSI.appending("-ql all ")
                    break
                }
                
                switch TSLReaderConfiguration.getSessionValue(scanMode: "multiScan") {
                case "S0":
                    commandRSSI = commandRSSI.appending("-qs s0 ")
                    break
                case "S1":
                    commandRSSI = commandRSSI.appending("-qs s1 ")
                    break
                case "S2":
                    commandRSSI = commandRSSI.appending("-qs s2 ")
                    break
                case "S3":
                    commandRSSI = commandRSSI.appending("-qs s3 ")
                    break
                default:
                    commandRSSI = commandRSSI.appending("-qs s1 ")
                    break
                }
                
                switch TSLReaderConfiguration.getTargetValue(scanMode: "multiScan") {
                case "A":
                    commandRSSI = commandRSSI.appending("-qt a ")
                    break
                case "B":
                    commandRSSI = commandRSSI.appending("-qt b ")
                    break
                default:
                    commandRSSI = commandRSSI.appending("-qt a ")
                    break
                }
                
                commandRSSI = commandRSSI.appending("-qv \(TSLReaderConfiguration.getQValue(scanMode: "multiScan")) -r off -sa 0 -sb epc -so 0000 -st s2 -tf off -x")
                
                print("commandRSSI", commandRSSI)
                commander.send(commandRSSI)
            }
        }
    }

    class func setRFIDSingleScanValues() {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        var commandRSSI = ".iv -al on -c off -dt off -e off -fi off -fs off -ie on -io on -ix off -n -o 29 -p -qa fix "
        if let commander = appDelegate.commander {
            if commander.isConnected {
                switch TSLReaderConfiguration.getSelectValue(scanMode: "singleScan") {
                case "No":
                    commandRSSI = commandRSSI.appending("-ql nsl ")
                    break
                case "Yes":
                    commandRSSI = commandRSSI.appending("-ql all ")
                    break
                default:
                    commandRSSI = commandRSSI.appending("-ql all ")
                    break
                }
                
                switch TSLReaderConfiguration.getSessionValue(scanMode: "singleScan") {
                case "S0":
                    commandRSSI = commandRSSI.appending("-qs s0 ")
                    break
                case "S1":
                    commandRSSI = commandRSSI.appending("-qs s1 ")
                    break
                case "S2":
                    commandRSSI = commandRSSI.appending("-qs s2 ")
                    break
                case "S3":
                    commandRSSI = commandRSSI.appending("-qs s3 ")
                    break
                default:
                    commandRSSI = commandRSSI.appending("-qs s1 ")
                    break
                }
                
                switch TSLReaderConfiguration.getTargetValue(scanMode: "singleScan") {
                case "A":
                    commandRSSI = commandRSSI.appending("-qt a ")
                    break
                case "B":
                    commandRSSI = commandRSSI.appending("-qt b ")
                    break
                default:
                    commandRSSI = commandRSSI.appending("-qt a ")
                    break
                }
                
                commandRSSI = commandRSSI.appending("-qv \(TSLReaderConfiguration.getQValue(scanMode: "singleScan")) -r off -sa 0 -sb epc -so 0000 -st s2 -tf off -x")
                print("commandRSSI", commandRSSI)
                commander.send(commandRSSI)
            }
        }
    }
    
    class func setRFIDFindItValues(epcTag: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        var commandRSSI = ".iv -al off -c off -dt off -e off -fi off -fs off -ie on -io off -ix off -n -o 29 -p -qa fix "
        if let commander = appDelegate.commander {
            if commander.isConnected {
                switch TSLReaderConfiguration.getSelectValue(scanMode: "geigerScan") {
                case "No":
                    commandRSSI = commandRSSI.appending("-ql nsl ")
                    break
                case "Yes":
                    commandRSSI = commandRSSI.appending("-ql all ")
                    break
                default:
                    commandRSSI = commandRSSI.appending("-ql all ")
                    break
                }
                
                switch TSLReaderConfiguration.getSessionValue(scanMode: "geigerScan") {
                case "S0":
                    commandRSSI = commandRSSI.appending("-qs s0 ")
                    break
                case "S1":
                    commandRSSI = commandRSSI.appending("-qs s1 ")
                    break
                case "S2":
                    commandRSSI = commandRSSI.appending("-qs s2 ")
                    break
                case "S3":
                    commandRSSI = commandRSSI.appending("-qs s3 ")
                    break
                default:
                    commandRSSI = commandRSSI.appending("-qs s1 ")
                    break
                }
                
                switch TSLReaderConfiguration.getTargetValue(scanMode: "geigerScan") {
                case "A":
                    commandRSSI = commandRSSI.appending("-qt a ")
                    break
                case "B":
                    commandRSSI = commandRSSI.appending("-qt b ")
                    break
                default:
                    commandRSSI = commandRSSI.appending("-qt a ")
                    break
                }
                
                commandRSSI = commandRSSI.appending("-qv \(TSLReaderConfiguration.getQValue(scanMode: "geigerScan")) -r on -sa 4 -sb epc -sd \(epcTag) -sl 60 -so 0020 -st s0 -tf off -x")
                print("commandRSSI", commandRSSI)
                commander.send(commandRSSI)
            }
        }
    }

    class func getBatteryValue() -> String {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let commander = appDelegate.commander {
            if commander.isConnected {
                if let batteryCommand = TSLBatteryStatusCommand.synchronousCommand() {
                    commander.execute(batteryCommand)
                    let status = String(format: " %i%@", batteryCommand.batteryLevel, "%")
                    return status
                }
            }
        }
        return "-"
    }
    
    class func setPowerLevelReader(readerValue: Float) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let commander: TSLAsciiCommander = appDelegate.commander {
            if commander.isConnected == true {
                if let command = TSLInventoryCommand.synchronousCommand() {
                    command.takeNoAction = TSL_TriState_YES
                    command.outputPower = Int32(TSLReaderConfiguration.getTslValue(valueIn: readerValue))
                    commander.execute(command)
                }
            }
        }
    }

    class func setVibrateReader() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let commander = appDelegate.commander {
            if commander.isConnected {
                let alertCommand : TSLAlertCommand = TSLAlertCommand()
                alertCommand.enableVibrator = TSLLocalConfig.getReaderVibrateValue() ?  TSL_TriState_YES : TSL_TriState_NO;
                commander.execute(alertCommand)
            }
        }
    }

    class func setSoundReader() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let commander = appDelegate.commander {
            if commander.isConnected {
                let alertCommand : TSLAlertCommand = TSLAlertCommand()
                alertCommand.enableBuzzer = TSLLocalConfig.getReaderSoundValue() ? TSL_TriState_YES : TSL_TriState_NO;
                commander.execute(alertCommand)
            }
        }
    }
    
    class func getTslValue(valueIn : Float) -> Float {
//        NSLog("TSL>>>> MAX SDK: \(TSLInventoryCommand.maximumOutputPower())")
//        NSLog("TSL>>>> MIN SDK: \(TSLInventoryCommand.minimumOutputPower())")
        let rangeOut : Float
        let minRSSI: Float = 10
        let maxRSSI: Float = 29
        let maximum : Float = 100
        rangeOut = Float(((Float(valueIn)/maximum) * (maxRSSI - minRSSI)) + minRSSI)
        return rangeOut
    }

    class func getRFDValue(valueIn : Float) -> Float {
        let rangeOut : Float
        let minRFD: Float = 120
        let maxRFD: Float = 300
        let maximum : Float = 100
        rangeOut = Float(((Float(valueIn)/maximum) * (maxRFD - minRFD)) + minRFD)
        return rangeOut
    }

    class func getRSSIValue(value: Int32) -> Int32 {
        let rangeOut : Float
        let minRFD: Float = -80
        let maxRFD: Float = -45
        let maximum : Float = 100
        rangeOut = Float(((Float(value)/maximum) * (maxRFD - minRFD)) + minRFD)
        return Int32(rangeOut)
    }
    
}
