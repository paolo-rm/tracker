//
//  TSLLocalConfig.swift
//  Tracker
//
//  Created by Paolo Ramos on 5/24/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

public enum MC_READER: String {
    case QGeigerScan = "QGeigerScan"
    case QMultiScan = "QMultiScan"
    case QSingleScan = "QSingleScan"
    case ReaderAutoConnect = "ReaderAutoConnect"
    case ReaderMultiplePower = "ReaderMultiplePower"
    case ReaderSinglePower = "ReaderSinglePower"
    case ReaderSoundEnabled = "ReaderSoundEnabled"
    case ReaderVibrateEnabled = "ReaderVibrateEnabled"
    case selectGeigerScan = "selectGeigerScan"
    case selectMultiScan = "selectMultiScan"
    case selectSingleScan = "selectSingleScan"
    case sessionGeigerScan = "sessionGeigerScan"
    case sessionMultiScan = "sessionMultiScan"
    case sessionSingleScan = "sessionSingleScan"
    case targetGeigerScan = "targetGeigerScan"
    case targetMultiScan = "targetMultiScan"
    case targetSingleScan = "targetSingleScan"
}

class TSLLocalConfig: NSObject {
    
    class var IS_TRUE: String { return "true" }
    
    class func removeAllPreferences() {
        let appDomain = Bundle.main.bundleIdentifier
        UserDefaults.standard.removePersistentDomain(forName: appDomain!)
    }
    
    class func getFirstTime() -> Bool {
        let defaults:UserDefaults = UserDefaults.standard
        return defaults.object(forKey: "first_time") as? Bool ?? Bool()
    }
    
    class func setFirstTime(bool: Bool) {
        let defaults: UserDefaults = UserDefaults.standard
        defaults.setValue(bool, forKey: "first_time")
    }
    
    class func getReaderAutoConnect() -> Bool {
        return UserDefaults.standard.bool(forKey: "autoConnect_reader")
    }
    
    class func setReaderAutoConnect(bool: Bool) {
        UserDefaults.standard.bool(forKey: "autoConnect_reader")
    }
    
    class func getReaderSoundValue() -> Bool {
        return UserDefaults.standard.bool(forKey: "sound_reader")
    }
    
    class func setReaderSoundValue(bool: Bool) {
        UserDefaults.standard.setValue(bool, forKey: "sound_reader")
    }
    
    class func getReaderVibrateValue() -> Bool {
        return UserDefaults.standard.bool(forKey: "vibrator_reader")
    }
    
    class func setReaderVibrateValue(bool: Bool) {
        UserDefaults.standard.setValue(bool, forKey: "vibrator_reader")
    }
    
    class func getMultiplePowerReader() -> Int {
        let defaults: UserDefaults = UserDefaults.standard
        let power : Int = defaults.integer(forKey: "multiple_power_reader")
        if power == 0 {
            return 50
        }
        return power
    }
    
    class func setMultiplePowerReader(value: Int) {
        let defaults: UserDefaults = UserDefaults.standard
        defaults.setValue(value, forKey: "multiple_power_reader")
    }
    
    class func getSimplePowerReader() -> Int {
        let defaults: UserDefaults = UserDefaults.standard
        let power: Int = defaults.integer(forKey: "simple_power_reader")
        if power == 0 {
            return 50
        }
        return power
    }
    
    class func setSimplePowerReader(value: Int) {
        let defaults: UserDefaults = UserDefaults.standard
        defaults.setValue(value, forKey: "simple_power_reader")
    }
    
    class func setControlButtonMode(value: String) {
        let defaults: UserDefaults = UserDefaults.standard
        defaults.setValue(value, forKey: "mode_saved")
    }
    class func setUserName(userName: String)  {
        let defaults: UserDefaults = UserDefaults.standard
        defaults.setValue(userName, forKey: "username_saved")
        
    }
    
    class func getUserName() -> String {
        let defaults: UserDefaults = UserDefaults.standard
        if let result = defaults.string(forKey: "username_saved") {
            return result
        }
        return ""
    }
    
    class func setPassword(userName: String) {
        let defaults: UserDefaults = UserDefaults.standard
        defaults.setValue(userName, forKey: "password_saved")
    }
    
    class func getPassword() -> String {
        let defaults: UserDefaults = UserDefaults.standard
        if let result = defaults.string(forKey: "password_saved") {
            return result
        }
        return ""
    }
    
    class func isChecked() -> Bool {
        let defaults: UserDefaults = UserDefaults.standard
        return defaults.bool(forKey: "checkbox")
    }
    
    class func setCheckStatus(value: Bool) {
        let defaults: UserDefaults = UserDefaults.standard
        defaults.setValue(value, forKey: "checkbox")
    }
    
    class func getCheckStatus() -> Bool {
        return UserDefaults.standard.bool(forKey: "checkbox")
    }
    
    class func getLastSync() -> String {
        if let lastSync = UserDefaults.standard.string(forKey: "lastSync") {
            return lastSync
        }
        return ""
    }
    
    class func setLastSync(value: String) {
        let defaults : UserDefaults = UserDefaults.standard
        defaults.setValue(value, forKey: "lastSync")
    }
    
}
