//
//  TRCStyleguide.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/3/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

// Color palette
extension UIColor {
    class var trcWhite: UIColor {
        return UIColor(white: 255.0 / 255.0, alpha: 1.0)
    }
    
    class var trcWhiteTwo: UIColor {
        return UIColor(white: 219.0 / 255.0, alpha: 0.25)
    }
    
    class var trcWhite10: UIColor {
        return UIColor(white: 255.0 / 255.0, alpha: 0.1)
    }
    
    class var trcBlack16: UIColor {
        return UIColor(white: 0.0, alpha: 0.16)
    }
    
    class var trcTealish: UIColor {
        return UIColor(red: 34.0 / 255.0, green: 182.0 / 255.0, blue: 216.0 / 255.0, alpha: 1.0)
    }
    
    class var trcDarkIndigo: UIColor {
        return UIColor(red: 13.0 / 255.0, green: 28.0 / 255.0, blue: 68.0 / 255.0, alpha: 1.0)
    }
    
    class var trcAqua: UIColor {
        return UIColor(red: 20.0 / 255.0, green: 191.0 / 255.0, blue: 225.0 / 255.0, alpha: 1.0)
    }
    
    class var trcTangerine: UIColor {
        return UIColor(red: 255.0 / 255.0, green: 134.0 / 255.0, blue: 12.0 / 255.0, alpha: 1.0)
    }
    
    class var trcSaffron: UIColor {
        return UIColor(red: 254.0 / 255.0, green: 184.0 / 255.0, blue: 13.0 / 255.0, alpha: 1.0)
    }
    
}

// Sample text styles
extension UIFont {
    class func trcHeaderFont() -> UIFont {
        return UIFont.systemFont(ofSize: 24.0, weight: UIFontWeightRegular)
    }
}

class TRCStyleguide: NSObject {

}
