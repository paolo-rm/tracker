//
//  SettingsViewController.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/21/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Class methods
    func configureView() {
        UtilView.getImageForBackground(name: Constant_Key.invBg, view: self.view)
        self.navigationController?.presentTransparentNavigationBar()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: Constants.idSettingTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.idSettingTableViewCell)
        self.tableView.register(UINib(nibName: Constants.idInformationTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.idInformationTableViewCell)
        self.tableView.separatorInset = UIEdgeInsets.zero
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        self.tableView.separatorColor = UIColor.clear
    }
    
    // MARK: Tablew View methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.idInformationTableViewCell, for: indexPath) as! InformationTableViewCell
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.idSettingTableViewCell, for: indexPath) as! SettingTableViewCell
            cell.titleLabel.text = "Reader Setting"
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.idSettingTableViewCell, for: indexPath) as! SettingTableViewCell
            cell.titleLabel.text = "About"
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.idSettingTableViewCell, for: indexPath) as! SettingTableViewCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: ViewController_Name.readerSettingsViewController) as! ReaderSettingsViewController
            vc.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.row == 2 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: ViewController_Name.aboutViewController) as! AboutViewController
            vc.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell: UITableViewCell
        if indexPath.row == 0 {
            cell = tableView.cellForRow(at: indexPath) as! InformationTableViewCell
        }
        else {
            cell = tableView.cellForRow(at: indexPath) as! SettingTableViewCell
            UtilView.getImageForBackground(name: Constant_Key.highlightedButton, view: cell.contentView)
            UtilView.getImageForBackground(name: Constant_Key.highlightedButton, view: cell)
        }
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell: UITableViewCell
        if indexPath.row == 0 {
            cell = tableView.cellForRow(at: indexPath) as! InformationTableViewCell
        }
        else {
            cell = tableView.cellForRow(at: indexPath) as! SettingTableViewCell
            UtilView.getImageForBackground(name: Constant_Key.invCellViewBg, view: cell.contentView)
            UtilView.getImageForBackground(name: Constant_Key.invCellViewBg, view: cell)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 100.0
        }
        return 65.0
    }
    

}
