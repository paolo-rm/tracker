//
//  InventoryScanViewController.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/13/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

class InventoryScanViewController: UIViewController {
    
    var setupScanView: SetupScanView!
    class var nextStep: String { return "nextStep" }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.configureView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.nextStep(notification:)),
                                               name: NSNotification.Name(rawValue: InventoryScanViewController.nextStep),
                                               object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: InventoryScanViewController.nextStep), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Class methods
    func configureView() {
        UtilView.getImageForBackground(name: Constant_Key.invBg, view: self.view)
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationItem.title = "REPORT ONE"
        
        self.setupScanView = SetupScanView(frame: UtilView.getContainerFrame(viewController: self))
        self.view.addSubview(self.setupScanView)
    }
    
    func nextStep(notification: NSNotification) {
        print("SCAN ITEMS")
        //        let vc = self.storyboard?.instantiateViewController(withIdentifier: ViewController_Name.inventoryReportViewController) as! InventoryReportViewController
        //        vc.modalTransitionStyle = .crossDissolve
        //        self.navigationController?.pushViewController(vc, animated: true)
    }
}
