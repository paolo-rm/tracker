//
//  FIReportListViewController.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/29/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

class FIReportListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Class methods
    func configureView() {
        UtilView.getImageForBackground(name: Constant_Key.invBg, view: self.view)
        UtilView.configureBackButton(vc: self, selector: #selector(self.goBack))
        self.navigationController?.presentTransparentNavigationBar()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: Constants.idLHeaderTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.idLHeaderTableViewCell)
        self.tableView.separatorInset = UIEdgeInsets.zero
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        self.tableView.separatorColor = UIColor.clear
        
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: Table View methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.idLHeaderTableViewCell, for: indexPath) as! LHeaderTableViewCell
            cell.firstLabel.text = "Serial"
            cell.secondLabel.text = "Last Detect"
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.idLHeaderTableViewCell, for: indexPath) as! LHeaderTableViewCell
            cell.firstLabel.text = "EA000000000000000002"
            cell.secondLabel.text = "Jan 1, 2017 8:59:59 AM"
            cell.firstLabel.textAlignment = .justified
            cell.firstLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightLight)
            cell.secondLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightLight)
            
            if indexPath.row % 2 == 0 {
                cell.firstLabel.backgroundColor = UIColor.trcWhiteTwo
                cell.secondLabel.backgroundColor = UIColor.trcWhiteTwo
            }
            else {
                cell.firstLabel.backgroundColor = UIColor.clear
                cell.secondLabel.backgroundColor = UIColor.clear
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        
        let findItAction = UITableViewRowAction(style: .normal, title: "\u{1F50D}\n Find It") { action, index in
            print("more button tapped")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: ViewController_Name.tagFinderViewController) as! TagFinderViewController
            vc.epcTag = "AE1000000000000000374241"
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        findItAction.backgroundColor = UIColor.trcSaffron
        
        let detailAction = UITableViewRowAction(style: .normal, title: "\u{1F6C8}\n Details") { action, index in
            print("favorite button tapped")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: ViewController_Name.InventoryDetailViewController) as! InventoryDetailViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        detailAction.backgroundColor = UIColor.trcAqua
        return [detailAction, findItAction]
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        NotificationCenter.default.post(name: Notification.Name(InventoryViewController.selectInventoryReport), object: nil)
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        if indexPath.row > 0 {
            let cell = tableView.cellForRow(at: indexPath) as! LHeaderTableViewCell
            UtilView.getImageForBackground(name: Constant_Key.highlightedButton, view: cell.contentView)
            UtilView.getImageForBackground(name: Constant_Key.highlightedButton, view: cell)
        }
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        if indexPath.row > 0 {
            let cell = tableView.cellForRow(at: indexPath) as! LHeaderTableViewCell
            UtilView.getImageForBackground(name: Constant_Key.invCellViewBg, view: cell.contentView)
            UtilView.getImageForBackground(name: Constant_Key.invCellViewBg, view: cell)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }


}
