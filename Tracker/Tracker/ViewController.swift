//
//  ViewController.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 4/29/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    class var loginNotification: String { return "LoginNotification" }
    var loginView: LoginView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }

    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.loginAction(notification:)),
                                               name: NSNotification.Name(rawValue: ViewController.loginNotification),
                                               object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: ViewController.loginNotification), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK: Class methods
    func configureView() {
        self.loginView = LoginView(frame: UtilView.getContainerFrame(viewController: self, notificationHeight: 0.0))
        self.view.addSubview(self.loginView)
    }
    
    func loginAction(notification: NSNotification) {
//        let username = self.loginView.userTextField.text ?? ""
//        let password = self.loginView.passwordTextField.text ?? ""
        // Method to control
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: ViewController_Name.tabBar) as! TabBarViewController
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
    }

}

