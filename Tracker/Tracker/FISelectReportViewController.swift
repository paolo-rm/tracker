//
//  FISelectReportViewController.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/29/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit
import SwiftyJSON

enum Section : Int {
    case Report
    case ScanMode
}
class FISelectReportViewController: UIViewController, SearchBarListViewDelegate {

    var searchBarListView: SearchBarListView!
    var arrayString = [String]()
    var checkControl = String()
    var module = Module.Inventory
    var section = Section.Report
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.configureView()
        self.searchBarListView.reloadSearchBarList(stringArrayData: self.arrayString, checkControl: self.checkControl)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Class methods
    func configureView() {
        UtilView.getImageForBackground(name: Constant_Key.invBg, view: self.view)
        UtilView.configureBackButton(vc: self, selector: #selector(self.goBack))
        self.navigationController?.presentTransparentNavigationBar()
        self.searchBarListView = SearchBarListView(frame: UtilView.getContainerFrame(viewController: self, notificationHeight: 60.0))
        self.view.addSubview(self.searchBarListView)
        self.searchBarListView.delegate = self
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func didSelectRowAtIndexPathInTableView(stringData: String, dictionaryData: JSON, isJSONData: Bool) {
        switch self.module {
        case .Inventory:
            break
        case .FindIt:
            if let viewController = self.navigationController!.viewControllers[1] as? FIReportViewController {
                if self.section == Section.Report {
                    viewController.reportString = stringData
                }
                if self.section == Section.ScanMode {
                    viewController.scanModeString = stringData
                }
            }
            break
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
