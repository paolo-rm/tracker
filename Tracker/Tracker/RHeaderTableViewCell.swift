//
//  ReportTableViewCell.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/13/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

class RHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var thirdLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        self.firstLabel.backgroundColor = UIColor.trcWhiteTwo
        self.secondLabel.backgroundColor = UIColor.trcWhiteTwo
        self.thirdLabel.backgroundColor = UIColor.trcWhiteTwo
        self.firstLabel.layer.borderWidth = 1.0
        self.secondLabel.layer.borderWidth = 1.0
        self.thirdLabel.layer.borderWidth = 1.0
        self.firstLabel.layer.borderColor = UIColor.trcBlack16.cgColor
        self.secondLabel.layer.borderColor = UIColor.trcBlack16.cgColor
        self.thirdLabel.layer.borderColor = UIColor.trcBlack16.cgColor
        self.tintColor = UIColor.white
        self.accessoryView?.tintColor = UIColor.white
        self.preservesSuperviewLayoutMargins = false
        self.separatorInset = UIEdgeInsets.zero
        self.layoutMargins = UIEdgeInsets.zero
    }
    
}
