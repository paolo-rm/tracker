//
//  Constants.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 4/30/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

struct Constant_Key {
    static let imageBackground = "Background"
    static let checkImage = "Check-Button"
    static let highlightedButton = "Highlighted-Button"
    static let invBg = "Inventory-Background"
    static let invCellViewBg = "cellView-Background"
    static let splashBg = "Splash-screen"
    static let iconReaderLow = "icon-0-reader"
    static let iconReaderMedium = "icon-50-reader"
    static let iconReader = "icon-reader"
    static let iconNoReader = "icon-no-reader"
    static let lineGradient = "Line-Gradient"
    static let iconDetail = "icon-detail"
    static let iconFind = "icon-find"
    static let mapPinBlue = "map-pin-blue"
    static let mapPinOrange = "map-pin-orange"
}

struct Xib_Name {
    static let loginView = "LoginView"
    static let inventoryView = "InventoryView"
    static let reportListView = "ReportListView"
    static let setupScanView = "SetupScanView"
    static let popoverPinView = "PopoverPinView"
    
}

struct ViewController_Name {
    static let tabBar = "tabBarViewController"
    static let inventoryReportViewController = "inventoryReportViewController"
    static let inventoryScanViewController = "inventoryScanViewController"
    static let aboutViewController = "AboutViewController"
    static let readerSettingsViewController = "ReaderSettingsViewController"
    static let TSLSelectReaderViewController = "TSLSelectReaderViewController"
    static let FIScanViewController = "FIScanViewController"
    static let FIScanTagViewController = "FIScanTagViewController"
    static let FITagFinderViewController = "FITagFinderViewController"
    static let InventoryDetailViewController = "inventoryDetailViewController"
    static let tagFinderViewController = "tagFinderViewController"
    static let FIReportViewController = "FIReportViewController"
    static let FISelectReportViewController = "FISelectReportViewController"
    static let FIReportListViewController = "FIReportListViewController"
    
}

struct Storyboard_Name {
    static let loginView = "Main"
}

class Constants: NSObject {
    // MARK: Login Status Message
    class var usernameStatus: String { return "User is empty" }
    class var passwortStatus: String { return "Password is empty" }
    class var userAndPasswordStatus: String { return "User or Password is empty" }
    class var userAndPasswordServerStatus: String { return "User or Password incorrect" }
    
    // MARK: register for nil view
    class var idOptionTableViewCell: String { return "OptionTableViewCell" }
    class var idRHeaderTableViewCell: String { return "RHeaderTableViewCell" }
    class var idRDetailTableViewCell: String { return "RDetailTableViewCell" }
    class var idSettingTableViewCell: String { return "SettingTableViewCell" }
    class var idInformationTableViewCell: String { return "InformationTableViewCell" }
    class var idReaderTableViewCell: String { return "ReaderTableViewCell" }
    class var idSwitchTableViewCell: String { return "SwitchTableViewCell" }
    class var idProgressTableViewCell: String { return "ProgressTableViewCell" }
    class var idSelectOptionTableViewCell: String { return "SelectOptionTableViewCell" }
    class var idTextFieldTableViewCell: String { return "TextFieldTableViewCell" }
    class var idDetailTableViewCell: String { return "DetailTableViewCell" }
    class var idLHeaderTableViewCell: String { return "LHeaderTableViewCell" }
    
}
