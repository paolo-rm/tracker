//
//  ReaderPreferences.swift
//  Tracker
//
//  Created by Paolo Ramos on 5/24/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit
import SwiftyJSON

class ReaderPreferences: NSObject {
    
    var readerThing: JSON = []
    
    static let _sharedInstance: ReaderPreferences = {
        let instance = ReaderPreferences()
        return instance
    }()
    
    class var sharedInstance: ReaderPreferences {
        return _sharedInstance
    }
    
    private override init() {
        super.init()
        self.readerThing = []
    }
    
    func setReaderThing(readerThingItem: JSON) {
        self.readerThing = readerThingItem
    }
    
    func getReaderAutoConnect() -> Bool {
        let value: String = self.readerThing[MC_READER.ReaderAutoConnect.rawValue].stringValue
        return value == TSLLocalConfig.IS_TRUE
    }
    
    func getQGeigerScan() -> String {
        let value: String = self.readerThing[MC_READER.QGeigerScan.rawValue].stringValue
        return value
    }

    func getQMultiScan() -> String {
        let value: String = self.readerThing[MC_READER.QMultiScan.rawValue].stringValue
        return value
    }
    
    func getQSingleScan() -> String {
        let value: String = self.readerThing[MC_READER.QSingleScan.rawValue].stringValue
        return value
    }
    
    func getReaderMultiplePower() -> String {
        let value: String = self.readerThing[MC_READER.ReaderMultiplePower.rawValue].stringValue
        return value
    }
    
    func getReaderSinglePower() -> String {
        let value: String = self.readerThing[MC_READER.ReaderSinglePower.rawValue].stringValue
        return value
    }
    
    func getReaderSoundEnabled() -> Bool {
        let value: String = self.readerThing[MC_READER.ReaderSoundEnabled.rawValue].stringValue
        return value == TSLLocalConfig.IS_TRUE
    }
    
    func getReaderVibrateEnabled() -> Bool {
        let value: String = self.readerThing[MC_READER.ReaderVibrateEnabled.rawValue].stringValue
        return value == TSLLocalConfig.IS_TRUE
    }
    
    func getSelectGeigerScan() -> String {
        let value: String = self.readerThing[MC_READER.selectGeigerScan.rawValue].stringValue
        return value
    }
    
    func getSelectMultiScan() -> String {
        let value: String = self.readerThing[MC_READER.selectMultiScan.rawValue].stringValue
        return value
    }
    
    func getSelectSingleScan() -> String {
        let value: String = self.readerThing[MC_READER.selectSingleScan.rawValue].stringValue
        return value
    }
    
    func getSessionGeigerScan() -> String {
        let value: String = self.readerThing[MC_READER.sessionGeigerScan.rawValue].stringValue
        return value
    }

    func getSessionMultiScan() -> String {
        let value: String = self.readerThing[MC_READER.sessionMultiScan.rawValue].stringValue
        return value
    }
    
    func getSessionSingleScan() -> String {
        let value: String = self.readerThing[MC_READER.sessionSingleScan.rawValue].stringValue
        return value
    }
    
    func getTargetGeigerScan() -> String {
        let value: String = self.readerThing[MC_READER.targetGeigerScan.rawValue].stringValue
        return value
    }

    func getTargetMultiScan() -> String {
        let value: String = self.readerThing[MC_READER.targetMultiScan.rawValue].stringValue
        return value
    }
    
    func getTargetSingleScan() -> String {
        let value: String = self.readerThing[MC_READER.targetSingleScan.rawValue].stringValue
        return value
    }
}
