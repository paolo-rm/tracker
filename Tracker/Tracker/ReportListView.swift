//
//  ReportListViewController.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/13/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

class ReportListView: UIView, UITableViewDelegate, UITableViewDataSource {
    
    let nibName = Xib_Name.reportListView
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addButton: UIButton!
    
    // Values
    var checkedBool = false
    
    // MARK: View Methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.xibSetup()
    }
    
    func xibSetup() {
        self.view = loadViewFromNib()
        self.view.frame = bounds
        self.view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        self.addSubview(self.view)
        self.configureView()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func configureView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: Constants.idRHeaderTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.idRHeaderTableViewCell)
        self.tableView.register(UINib(nibName: Constants.idRDetailTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.idRDetailTableViewCell)
        self.tableView.separatorInset = UIEdgeInsets.zero
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        self.tableView.separatorColor = UIColor.clear
        UtilView.configButtonGradientStyle(button: self.addButton)
    }
    
    // MARK: Table View Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.idRHeaderTableViewCell, for: indexPath) as! RHeaderTableViewCell
            cell.firstLabel.text = "Item Name"
            cell.secondLabel.text = "EPC"
            cell.thirdLabel.text = "Location"
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.idRDetailTableViewCell, for: indexPath) as! RDetailTableViewCell
            cell.firstLabel.text = "Chair 1"
            cell.secondLabel.text = "EA000000000000000002"
            cell.thirdLabel.text = "Zone 4"
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        NotificationCenter.default.post(name: Notification.Name(InventoryViewController.selectInventoryReport), object: nil)
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        if indexPath.row > 0 {
            let cell = tableView.cellForRow(at: indexPath) as! RDetailTableViewCell
            UtilView.getImageForBackground(name: Constant_Key.highlightedButton, view: cell.contentView)
            UtilView.getImageForBackground(name: Constant_Key.highlightedButton, view: cell)
        }
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        if indexPath.row > 0 {
            let cell = tableView.cellForRow(at: indexPath) as! RDetailTableViewCell
            UtilView.getImageForBackground(name: Constant_Key.invCellViewBg, view: cell.contentView)
            UtilView.getImageForBackground(name: Constant_Key.invCellViewBg, view: cell)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }

    // MARK: Scan Items
    @IBAction func scanItems(_ sender: UIButton) {
        NotificationCenter.default.post(name: Notification.Name(InventoryReportViewController.scanItems), object: nil)
    }

}
