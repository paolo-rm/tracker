//
//  InventoryView.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/1/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit
import Popover

enum Module : Int {
    case Inventory
    case FindIt
}

class InventoryView: UIView, UITableViewDelegate, UITableViewDataSource {
    
    let nibName = Xib_Name.inventoryView
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonScan: UIButton!
    
    class var None: String { return "None" }
    var module = Module.Inventory
    var reportString = InventoryView.None
    var scanModeString = InventoryView.None
    
    // Values
    var checkedBool = false
    
    // MARK: View Methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.xibSetup()
    }
    
    func xibSetup() {
        self.view = loadViewFromNib()
        self.view.frame = bounds
        self.view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        self.addSubview(self.view)
        self.configureView()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func configureView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: Constants.idOptionTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.idOptionTableViewCell)
        self.tableView.separatorInset = UIEdgeInsets.zero
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        self.tableView.separatorColor = UIColor.clear
        UtilView.configButtonGradientStyle(button: self.buttonScan)
        
    }
    
    func updateModule(moduleValue: Module, report: String, scanMode: String) {
        self.module = moduleValue
        self.reportString = report
        self.scanModeString = scanMode
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.tableView.reloadData()
                switch self.module {
                case .Inventory:
                    self.buttonScan.setTitle("Star Scanning", for: .normal)
                    break
                case .FindIt:
                    self.buttonScan.setTitle("Next", for: .normal)
                    break
                }
            }
        }
    }
    // MARK: Table View Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.idOptionTableViewCell, for: indexPath) as! OptionTableViewCell
        if indexPath.row == 0 {
            cell.titleLabel.text = "Report"
            cell.detailLabel.text = self.reportString
        }
        
        if indexPath.row == 1 {
            cell.titleLabel.text = "Scan Mode"
            cell.detailLabel.text = self.scanModeString
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch self.module {
        case .Inventory:
            if indexPath.row == 0 {
                NotificationCenter.default.post(name: Notification.Name(InventoryViewController.selectInventoryReport), object: nil)
            }
            
            break
        case .FindIt:
            if indexPath.row == 0 {
                NotificationCenter.default.post(name: Notification.Name(FIReportViewController.selectFIReport), object: nil)
            }
            if indexPath.row == 1 {
                NotificationCenter.default.post(name: Notification.Name(FIReportViewController.selectFIScanMode), object: nil)
            }
            break
        }
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! OptionTableViewCell
        UtilView.getImageForBackground(name: Constant_Key.highlightedButton, view: cell.contentView)
        UtilView.getImageForBackground(name: Constant_Key.highlightedButton, view: cell)
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! OptionTableViewCell
        UtilView.getImageForBackground(name: Constant_Key.invCellViewBg, view: cell.contentView)
        UtilView.getImageForBackground(name: Constant_Key.invCellViewBg, view: cell)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    
    // MARK: Select Button
    @IBAction func starScannig(_ sender: UIButton) {
        switch self.module {
        case .Inventory:
            
            break
        case .FindIt:
            NotificationCenter.default.post(name: Notification.Name(FIReportViewController.goNext), object: nil)
            break
        }
    }
    
}
