//
//  UtilView.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 4/30/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

// ViewController extension
extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}

// UIButton extension
extension UIButton {
    func setBackgroundColor(color: UIColor, forState: UIControlState) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.setBackgroundImage(colorImage, for: forState)
    }
}

extension UIButton {
    func alignVertical(spacing: CGFloat = 6.0) {
        guard let imageSize = self.imageView?.image?.size,
            let text = self.titleLabel?.text,
            let font = self.titleLabel?.font
            else { return }
        self.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -imageSize.width, bottom: -(imageSize.height + spacing), right: 0.0)
        let labelString = NSString(string: text)
        let titleSize = labelString.size(attributes: [NSFontAttributeName: font])
        self.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + spacing), left: 0.0, bottom: 0.0, right: -titleSize.width)
        let edgeOffset = abs(titleSize.height - imageSize.height) / 2.0;
        self.contentEdgeInsets = UIEdgeInsets(top: edgeOffset, left: 0.0, bottom: edgeOffset, right: 0.0)
    }
}

// Clear Navigation Controller
extension UINavigationController {
    public func presentTransparentNavigationBar() {
        navigationBar.setBackgroundImage(UIImage(), for:UIBarMetrics.default)
        navigationBar.isTranslucent = true
        navigationBar.shadowImage = UIImage()
        setNavigationBarHidden(false, animated:true)
    }
    
    public func hideTransparentNavigationBar() {
        setNavigationBarHidden(true, animated:false)
        navigationBar.setBackgroundImage(UINavigationBar.appearance().backgroundImage(for: UIBarMetrics.default), for:UIBarMetrics.default)
        navigationBar.isTranslucent = UINavigationBar.appearance().isTranslucent
        navigationBar.shadowImage = UINavigationBar.appearance().shadowImage
    }
}

// UIVIew extensions
// Usage: insert view.fadeTransition right before changing content
extension UIView {
    func fadeTransition(_ duration:CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            kCAMediaTimingFunctionEaseInEaseOut)
        animation.type = kCATransitionFade
        animation.duration = duration
        layer.add(animation, forKey: kCATransitionFade)
    }
}

class UtilView: NSObject {

    // Get image Color
    class func imageColor(color: UIColor) -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return colorImage ?? UIImage()
    }
    // Configure Title Navigation Bar
    class func configureTitleView(viewController: UIViewController, nameXib: String) {
        if let navTitleView : UIView = Bundle.main.loadNibNamed(nameXib, owner: nil, options: nil)![0] as? UIView {
            viewController.navigationItem.titleView = navTitleView
        }
    }
    
    // Get Image from View
    class func getImageForBackground(name: String, view: UIView) {
        if let image = UIImage(named: name) {
            UIGraphicsBeginImageContext(view.frame.size)
            image.draw(in: view.bounds)
            let imageBackground: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            view.backgroundColor = UIColor(patternImage: imageBackground)
        }
    }
    
    // Configure View
    class func configViewGradientStyle(view: UIView) {
        
        view.backgroundColor = UIColor.clear
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.bounds
        let color1 = UIColor.trcDarkIndigo.withAlphaComponent(0.80).cgColor
        let color2 = UIColor.trcAqua.withAlphaComponent(0.30).cgColor
        gradientLayer.colors = [color1, color2]
        gradientLayer.locations = [0.0, 1.0]
        view.layer.addSublayer(gradientLayer)
    }

    // Configure Button
    class func configButtonGradientStyle(button: UIButton) {
        
        let gradient = CAGradientLayer()
        gradient.frame =  button.bounds
        gradient.colors = [UIColor.white.withAlphaComponent(0.10).cgColor, UIColor.white.cgColor, UIColor.white.withAlphaComponent(0.10).cgColor]
        gradient.startPoint = CGPoint(x: 1, y: 0)
        gradient.endPoint = CGPoint(x: 0, y: 0)
        gradient.cornerRadius = 5.0
        
        let shape = CAShapeLayer()
        shape.lineWidth = 2
        shape.borderWidth = 2
        shape.cornerRadius = button.layer.cornerRadius
        shape.path = UIBezierPath(rect: button.bounds).cgPath
        shape.strokeColor = UIColor.black.cgColor
        shape.fillColor = UIColor.clear.cgColor
        gradient.mask = shape
        
        button.layer.addSublayer(gradient)
        button.tintColor = UIColor.trcWhite
        
    }
    
    class func configButtonStyle(button: UIButton) {
        button.layer.cornerRadius = 5.0
        button.layer.borderWidth = 1.0
        button.layer.borderColor = UIColor.white.cgColor
    }
    
    class func configButtonCheckStyle(button: UIButton) {
        button.layer.borderWidth = 1.0
        button.layer.borderColor = UIColor.white.cgColor
    }
    
    class func configureBackButton(vc: UIViewController, selector: Selector) {
        vc.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "Back"), style: .plain, target: vc, action: selector)
    }
    
    // Configure TextField
    class func setPlaceholderColor(textField: UITextField) {
        textField.attributedPlaceholder = NSAttributedString(string: textField.placeholder!, attributes: [NSForegroundColorAttributeName : UIColor.white])
    }
    
    // Configure View Frame
    class func getContainerFrame(viewController: UIViewController) -> CGRect {
        let notificationHeight = CGFloat(20.0)
        let height = viewController.navigationController?.navigationBar.frame.size.height ?? 0.0
        let tabBarHeight = viewController.tabBarController?.tabBar.frame.height ?? 0.0
        return CGRect(x: viewController.view.frame.origin.x,
                      y: viewController.view.frame.origin.y + height + notificationHeight,
                      width: viewController.view.frame.width,
                      height: viewController.view.frame.height - tabBarHeight - height - notificationHeight)
    }
    
    class func getContainerFrame(viewController: UIViewController, notificationHeight: CGFloat) -> CGRect {
        let height = viewController.navigationController?.navigationBar.frame.size.height ?? 0.0
        let tabBarHeight = viewController.tabBarController?.tabBar.frame.height ?? 0.0
        return CGRect(x: viewController.view.frame.origin.x,
                      y: viewController.view.frame.origin.y + height + notificationHeight,
                      width: viewController.view.frame.width,
                      height: viewController.view.frame.height - tabBarHeight - height - notificationHeight)
    }
    
    // Resize image
    class func imageWithImage (sourceImage:UIImage, scaledToWidth: CGFloat) -> UIImage {
        let oldWidth = sourceImage.size.width
        let scaleFactor = scaledToWidth / oldWidth
        
        let newHeight = sourceImage.size.height * scaleFactor
        let newWidth = oldWidth * scaleFactor
        
        UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
        sourceImage.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
