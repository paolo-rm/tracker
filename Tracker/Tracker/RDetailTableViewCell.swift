//
//  RDetailTableViewCell.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/13/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

class RDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var firstLabel: InsetLabel!
    @IBOutlet weak var secondLabel: InsetLabel!
    @IBOutlet weak var thirdLabel: InsetLabel!
    let inset: CGFloat = 5.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }
    
}
