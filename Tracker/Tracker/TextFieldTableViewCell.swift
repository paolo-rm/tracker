//
//  TextFieldTableViewCell.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/26/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

class TextFieldTableViewCell: UITableViewCell {

    @IBOutlet weak var textField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        UtilView.getImageForBackground(name: Constant_Key.invCellViewBg, view: self)
        self.textField.backgroundColor = UIColor.clear
        self.textField.attributedPlaceholder = NSAttributedString(string: "Insert code ...", attributes: [NSForegroundColorAttributeName: UIColor.lightGray])
        self.tintColor = UIColor.white
        self.preservesSuperviewLayoutMargins = false
        self.separatorInset = UIEdgeInsets.zero
        self.layoutMargins = UIEdgeInsets.zero
        
    }
    
}
