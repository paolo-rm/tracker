//
//  SetupScanView.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/13/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit


class SetupScanView: UIView, UITableViewDelegate, UITableViewDataSource {
    
    let nibName = Xib_Name.setupScanView
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonScan: UIButton!
    
    // Values
    var checkedBool = false
    
    // MARK: View Methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.xibSetup()
    }
    
    func xibSetup() {
        self.view = loadViewFromNib()
        self.view.frame = bounds
        self.view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        self.addSubview(self.view)
        self.configureView()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func configureView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: Constants.idOptionTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.idOptionTableViewCell)
        self.tableView.separatorInset = UIEdgeInsets.zero
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        self.tableView.separatorColor = UIColor.clear
        
        UtilView.configButtonGradientStyle(button: self.buttonScan)
    }
    
    // MARK: Table View Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.idOptionTableViewCell, for: indexPath) as! OptionTableViewCell
        cell.titleLabel.text = "\(indexPath.row)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! OptionTableViewCell
        UtilView.getImageForBackground(name: Constant_Key.highlightedButton, view: cell.contentView)
        UtilView.getImageForBackground(name: Constant_Key.highlightedButton, view: cell)
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! OptionTableViewCell
        UtilView.getImageForBackground(name: Constant_Key.invCellViewBg, view: cell.contentView)
        UtilView.getImageForBackground(name: Constant_Key.invCellViewBg, view: cell)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    
    // MARK: Scan Items
    @IBAction func nextStep(_ sender: UIButton) {
        NotificationCenter.default.post(name: Notification.Name(InventoryScanViewController.nextStep), object: nil)
    }

}
