//
//  LoginView.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 4/29/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

class LoginView: UIView, UITextFieldDelegate {

    let nibName = Xib_Name.loginView
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var statusMessage: UILabel!
    
    // Values
    var checkedBool = false
    
    // MARK: View Methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.xibSetup()
    }
    
    func xibSetup() {
        self.view = loadViewFromNib()
        self.view.frame = bounds
        self.view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        self.addSubview(self.view)
        self.configureView()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func configureView() {
        UtilView.getImageForBackground(name: Constant_Key.imageBackground, view: self.view)
        UtilView.configButtonGradientStyle(button: self.loginButton)
        UtilView.configButtonCheckStyle(button: self.checkBoxButton)
        UtilView.setPlaceholderColor(textField: self.userTextField)
        UtilView.setPlaceholderColor(textField: self.passwordTextField)
        
        self.userTextField.delegate = self
        self.passwordTextField.delegate = self
        
        self.userTextField.text = "Paolo"
        self.passwordTextField.text = "paolo"
    }
    
    func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.userTextField.resignFirstResponder()
        self.passwordTextField.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    // MARK: TextField methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.userTextField {
            if let text = self.userTextField.text {
                if text.isEmpty {
                    self.statusMessage.text = Constants.usernameStatus
                }
                else {
                    self.statusMessage.text = ""
                    return self.passwordTextField.becomeFirstResponder()
                }
            }
        }
        
        if textField == self.passwordTextField {
            if let text = self.passwordTextField.text {
                if text.isEmpty {
                    self.statusMessage.text = Constants.passwortStatus
                }
                else {
                    self.statusMessage.text = ""
                    self.loginMethod()
                }
            }
        }
        
        self.view.endEditing(true)
        return false
    }
    
    // MARK: Login action
    @IBAction func buttonReleased(_ sender: UIButton) {
        self.loginButton.backgroundColor = UIColor.trcAqua
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
        self.loginButton.backgroundColor = UIColor.clear
        let username = self.userTextField.text ?? ""
        let password = self.passwordTextField.text ?? ""
        if !username.isEmpty && !password.isEmpty {
            self.statusMessage.text = ""
            self.loginMethod()
        }
        else {
            self.statusMessage.text = Constants.userAndPasswordStatus
        }
    }
    
    func loginMethod() {
        NotificationCenter.default.post(name: Notification.Name(ViewController.loginNotification), object: nil)
    }
    
    // MARK: Remember me methods
    @IBAction func checkAction(_ sender: UIButton) {
        self.checkedBool = !checkedBool
        if self.checkedBool {
            sender.setImage(UIImage(named: Constant_Key.checkImage), for: .normal)
        }
        else {
            sender.setImage( UIImage(), for: .normal)
        }
    }
    
}
