//
//  FITagFinderViewController.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/27/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit
import KDCircularProgress

class FITagFinderViewController: ViewController, TSLCommandConfigureDelegate {
    
    @IBOutlet weak var EPCLabel: UILabel!
    @IBOutlet weak var progress: CustomSlider!
    @IBOutlet weak var powerLabel: UILabel!
    @IBOutlet weak var percentageLabel: UILabel!
    @IBOutlet weak var detailsButton: UIButton!
    @IBOutlet weak var progressView: UIView!
    var circularProgress: KDCircularProgress!
    
    // Variables to Find Item
    var readerCommand: TSLCommandConfigure!
    var minRSSI: Float = 75
    var maxRSSI: Float = 42
    let maximum : Float = 100
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.configureView()
        self.configureProgressView()
        self.EPCLabel.text = "AE1000000000000000374241"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.readerCommand = TSLCommandConfigure(epcTag: self.EPCLabel.text ?? "")
        self.readerCommand.delegate = self
        TSLReaderConfiguration.setPowerLevelReader(readerValue: self.progress.value)
        self.updateProgress(value: 0)
    }
    
    // MARK: Class Methods
    override func configureView() {
        UtilView.getImageForBackground(name: Constant_Key.invBg, view: self.view)
        UtilView.configureBackButton(vc: self, selector: #selector(self.goBack))
        UtilView.configButtonGradientStyle(button: self.detailsButton)
        self.navigationController?.presentTransparentNavigationBar()
    }
    
    func configureProgressView() {
        self.circularProgress = KDCircularProgress(frame: self.progressView.frame)
        self.circularProgress.startAngle = -90
        self.circularProgress.progressThickness = 0.3
        self.circularProgress.trackThickness = 0.3
        self.circularProgress.trackColor = UIColor.trcWhite10
        self.circularProgress.clockwise = true
        self.circularProgress.gradientRotateSpeed = 2
        self.circularProgress.roundedCorners = false
        self.circularProgress.glowMode = .forward
        self.circularProgress.glowAmount = 0.9
        self.circularProgress.set(colors: UIColor.trcTangerine, UIColor.trcSaffron)
        self.view.addSubview(self.circularProgress)
        self.circularProgress.animate(fromAngle: self.circularProgress.angle, toAngle: 0, duration: 0.1, completion: nil)
    }

    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func outPutPowerEditingDidEnd(_ sender: CustomSlider) {
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                let value:Int = Int(sender.value)
                sender.setValue(Float(value), animated: true)
                TSLReaderConfiguration.setPowerLevelReader(readerValue: sender.value)
            }
        }
    }
    
    @IBAction func powerProgressValueChanged(_ sender: CustomSlider) {
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.powerLabel.text = "Power \(Int(sender.value))%"
                self.powerLabel.fadeTransition(0.2)
            }
        }
    }
    
    @IBAction func goDetailsView(_ sender: UIButton) {
    }
    
    // MARK: TLS Methods
    func TSLFindItTransponderReceived(epc: String!, crc: NSNumber!, pc: NSNumber!, rssi: NSNumber!, fastId: Data!, moreAvailable: Bool) {
        print("epc :\(epc ?? ""), crc: \(crc), pc: \(pc), rssi: \(rssi) , fastId: \(fastId)")
        let epcTag = EPCLabel.text ?? ""
        if epcTag  == epc {
            let newTag: Tag = Tag()
            newTag.epc = epc as String
            newTag.rssi = rssi
            let value:Int = Int(newTag.rssi)
            
            if ((Float(value * -1)) > self.minRSSI) {
                self.minRSSI = (Float(value * -1))
            }
            
            if ((Float(value * -1)) < self.maxRSSI) {
                self.maxRSSI = (Float(value * -1)) + 3.0
            }
            
            let range : Float = (((Float(value * -1) - (minRSSI)) / (maxRSSI - minRSSI)) * maximum)
            
            let rangeInt: Int = Int(range)
            print("RANGE IT >>>> \(rangeInt)")
            self.updateProgress(value: rangeInt)
        }
        Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(self.tagWasFoundOff), userInfo: nil, repeats: false)
    }
    
    func tagWasFoundOff()
    {
        self.circularProgress.animate(fromAngle: self.circularProgress.angle, toAngle: 0, duration: 0.2, completion: nil)
    }
    
    func updateProgress(value: Int) {
        // Circular Progress
        var newValue = value
        if value >= 100 {
           newValue = 100
        }
        let valueToCP = Double((newValue * 360) / 100)
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.circularProgress.animate(toAngle: valueToCP, duration: 0.5, completion: nil)
                self.percentageLabel.fadeTransition(0.2)
                self.percentageLabel.text = "\(newValue)%"
            }
        }
    }
    
}
