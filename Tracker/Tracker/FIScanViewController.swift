//
//  FIScanViewController.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/26/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

class FIScanViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var button: UIButton!
    var scanIt = "Scan it"
    var scanTagString = "Scan it"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Class Methods
    func configureView() {
        UtilView.getImageForBackground(name: Constant_Key.invBg, view: self.view)
        UtilView.configureBackButton(vc: self, selector: #selector(self.goBack))
        self.navigationController?.presentTransparentNavigationBar()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: Constants.idOptionTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.idOptionTableViewCell)
        self.tableView.register(UINib(nibName: Constants.idTextFieldTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.idTextFieldTableViewCell)
        self.tableView.separatorInset = UIEdgeInsets.zero
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        self.tableView.separatorColor = UIColor.clear
        
        UtilView.configButtonGradientStyle(button: self.button)
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: Table View Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        if section == 1 {
            let header = view as! UITableViewHeaderFooterView
            header.backgroundView?.backgroundColor = UIColor.clear
            header.textLabel?.font = UIFont(name: "System", size: 17)
            header.textLabel?.textAlignment = NSTextAlignment.center
            header.textLabel?.textColor = UIColor.white
            header.textLabel?.text = "or Type your code"
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 65.0
        }
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.idOptionTableViewCell, for: indexPath) as! OptionTableViewCell
            cell.titleLabel.text = "EPC Barcode"
            cell.detailLabel.text = self.scanTagString
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.idTextFieldTableViewCell, for: indexPath) as! TextFieldTableViewCell
            cell.textField.delegate = self
            cell.textField.text = self.scanTagString
            if self.scanTagString == self.scanIt {
                cell.textField.text = ""
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: ViewController_Name.FIScanTagViewController) as! FIScanTagViewController
            vc.checkControl = self.scanTagString
            vc.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let cell = tableView.cellForRow(at: indexPath) as! OptionTableViewCell
            UtilView.getImageForBackground(name: Constant_Key.highlightedButton, view: cell.contentView)
            UtilView.getImageForBackground(name: Constant_Key.highlightedButton, view: cell)
        }
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let cell = tableView.cellForRow(at: indexPath) as! OptionTableViewCell
            UtilView.getImageForBackground(name: Constant_Key.invCellViewBg, view: cell.contentView)
            UtilView.getImageForBackground(name: Constant_Key.invCellViewBg, view: cell)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    
    // MARK: TextField methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.scanTagString = textField.text ?? ""
                self.tableView.reloadData()
                textField.resignFirstResponder()
            }
        }
        return true
    }
    // MARK: Select Button
    @IBAction func goFindIt(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: ViewController_Name.tagFinderViewController) as! TagFinderViewController
        vc.epcTag = "AE1000000000000000374241"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
