//
//  FindItemViewController.swift
//  Tracker
//
//  Created by Paolo Ramos on 5/25/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit
import Popover
import MRProgress

class FindItemViewController: UIViewController {
    
    @IBOutlet weak var scanButton: UIButton!
    @IBOutlet weak var listButton: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Class Methods
    func configureView() {
        UtilView.getImageForBackground(name: Constant_Key.invBg, view: self.view)
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.isNavigationBarHidden = true
        self.scanButton.alignVertical()
        self.listButton.alignVertical()
    }
    
    @IBAction func goScanTag(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: ViewController_Name.FIScanViewController) as! FIScanViewController
        vc.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func goTagList(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: ViewController_Name.FIReportViewController) as! FIReportViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
