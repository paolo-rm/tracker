//
//  TabBarViewController.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 4/30/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Class methods
    func configureView() {
        self.tabBar.barTintColor = UIColor.white
        self.tabBar.backgroundImage = UtilView.imageColor(color: UIColor.clear)
        let frost = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        frost.frame = self.tabBar.bounds
        frost.autoresizingMask = .flexibleWidth
        self.tabBar.insertSubview(frost, at: 0)
    }
    
}
