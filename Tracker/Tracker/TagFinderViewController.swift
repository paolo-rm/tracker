//
//  TagFinderViewController.swift
//  vizix-ios-project
//
//  Created by Fabiola Ramirez on 9/1/16.
//  Copyright © 2015    Mojix. All rights reserved.
//

import UIKit
import AVFoundation

class TagFinderViewController: UIViewController, TSLInventoryCommandTransponderReceivedDelegate, TSLSwitchResponderStateReceivedDelegate {
    
    //RFID Readers
    var commander:TSLAsciiCommander?
    var commander1:TSLAsciiCommander?
    var inventoryResponder:TSLInventoryCommand?
    var ssCommand:TSLSwitchStateCommand?
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var partialResultMessage:String = ""
    var switchResponder:TSLSwitchResponder?
    var pollingTimer:Timer?
    var scanTimer:Timer?
    var tags:NSMutableArray = NSMutableArray()
    var epcTag:String = "hex000000000000000000000"
    
    var dictionaryObjectDetail:Dictionary<String, AnyObject>!
    var arrayKeys:[String]!
    var arrayData:[AnyObject]!
    
    var firstBeep : AVAudioPlayer?
    var secondBeep : AVAudioPlayer?
    var thirdBeep : AVAudioPlayer?
    var fourthBeep : AVAudioPlayer?
    
    var soundIsOn = true
    var tagWasFound = false
    var scanningButton = false
    @IBOutlet weak var outputTextLabel: UILabel!
    @IBOutlet weak var outputSliderLabel: UILabel!
    @IBOutlet weak var sliderView: UISlider!
    
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var soundIdSwitch: UISwitch!
    @IBOutlet weak var oneView: UIView!
    @IBOutlet weak var twoView: UIView!
    @IBOutlet weak var threeView: UIView!
    @IBOutlet weak var fourView: UIView!
    @IBOutlet weak var fiveView: UIView!
    @IBOutlet weak var sixView: UIView!
    @IBOutlet weak var sevenView: UIView!
    @IBOutlet weak var eightView: UIView!
    @IBOutlet weak var nineView: UIView!
    @IBOutlet weak var tenView: UIView!
    
    @IBOutlet weak var scanButton: UIButton!
    @IBOutlet weak var detailsButton: UIButton!
    
    var minRSSI: Float = 75
    var maxRSSI: Float = 42
    let maximum : Float = 100
    
    // MARK: View Controller Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let button = UIBarButtonItem.init(image: UIImage(named: "Back"), style: .plain, target: self, action: #selector(TagFinderViewController.goBack))
        self.navigationItem.leftBarButtonItem = button
        
        self.scanButton.addTarget(self, action: #selector(TagFinderViewController.scanB(_:)), for: UIControlEvents.touchUpInside)
        self.configureView()

        //Get Commander from appDelegate
        commander = appDelegate.commander
        commander1 = appDelegate.commander
        partialResultMessage = ""
        
        switchResponder = TSLSwitchResponder.default()
        switchResponder?.switchStateReceivedDelegate = self
        commander?.add(switchResponder)
        //commander?.addSynchronousResponder()
        
        print("EPC To Find-> \(epcTag)")
        
        self.outputTextLabel.text = String(format: "%@%@","EPC: ",epcTag)
        self.view.bringSubview(toFront: self.sliderView)
        self.viewCustomeDefault()
        
        if let firstBeep = self.setupAudioPlayerWithFile(file: "Quietest", type:"wav") {
            self.firstBeep = firstBeep
        }
        if let secondBeep = self.setupAudioPlayerWithFile(file: "Default", type:"wav") {
            self.secondBeep = secondBeep
        }
        if let thirdBeep = self.setupAudioPlayerWithFile(file: "Loudest", type:"wav") {
            self.thirdBeep = thirdBeep
        }
        if let fourthBeep = self.setupAudioPlayerWithFile(file: "Success", type:"wav") {
            self.fourthBeep = fourthBeep
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        print("VIEW WILL APPEAR METHOD")
        self.scanButton.isEnabled = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                print("Cleaning Responders")
                if self.commander?.isConnected == true {
                    self.commander?.clearResponders()
                    //This enables debugging output from reader
                    self.commander?.addSynchronousResponder();
                    self.inventoryResponder = TSLInventoryCommand()
                    self.inventoryResponder?.transponderReceivedDelegate = self
                    self.inventoryResponder?.captureNonLibraryResponses = true
                    self.inventoryResponder?.includeTransponderRSSI = TSL_TriState_YES
                    self.commander?.add(self.inventoryResponder)
                    TSLReaderConfiguration.setRFIDFindItValues(epcTag: self.epcTag)
                    let saCommand = TSLSwitchActionCommand.synchronousCommand()
                    saCommand?.asynchronousReportingEnabled = TSL_TriState_NO
                    saCommand?.resetParameters = TSL_TriState_YES
                    self.commander?.execute(saCommand)
                }
                self.updateScanButtonState()
                self.pollingTimer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(TagFinderViewController.pollTrigger), userInfo: nil, repeats: true)
            }
        }
    }
    
    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated);
        self.scanTimer?.invalidate()
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                print("Cleaning Responders")
                if self.commander?.isConnected == true {
                    TSLReaderConfiguration.setRFIDMultipleScanValues()
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("viewDidDisappear")
        super.viewDidDisappear(animated);
        pollingTimer?.invalidate()
        self.scanButton.isSelected = !self.scanButton.isSelected
    }
    
    override func willMove(toParentViewController parent: UIViewController?) {
        print("willMoveToParentViewController")
        super.willMove(toParentViewController: parent)
        if parent == nil {
            print("=====> This VC is 'will' be popped. i.e. the back button was pressed.")
        }
    }
    
    deinit {
        TSLReaderConfiguration.setFactoryDefault()
    }
    
    func updateScanButtonState() {
        if self.commander?.isConnected == true {
            self.scanButton.isEnabled = true
            if self.scanningButton {
                self.scanB(self.scanButton)
            }
        }
        else {
            self.scanButton.isEnabled = false
        }
    }
    
    func goBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Class Methods
    func configureView() {
        UtilView.getImageForBackground(name: Constant_Key.invBg, view: self.view)
        self.navigationController?.presentTransparentNavigationBar()
        UtilView.configButtonGradientStyle(button: self.scanButton)
        UtilView.configButtonGradientStyle(button: self.detailsButton)
    }
    
    func setupAudioPlayerWithFile(file:NSString, type:NSString) -> AVAudioPlayer?  {
        //1
        let path = Bundle.main.path(forResource: file as String, ofType: type as String)
        let url = NSURL.fileURL(withPath: path!)
        
        //2
        var audioPlayer:AVAudioPlayer?
        
        // 3
        do {
            try audioPlayer = AVAudioPlayer(contentsOf: url)
        } catch {
            print("Player not available")
        }
        
        return audioPlayer
    }
    
    @IBAction func soundSwitchValueChanged(_ sender: UISwitch) {
        if (sender.isOn){
            print("on")
            self.soundIsOn = true
        }
        else{
            self.soundIsOn = false
        }
    }
    
    func transponderReceived(_ epc: String!, crc: NSNumber!, pc: NSNumber!, rssi: NSNumber!, fastId: Data!, moreAvailable: Bool) {
        self.tagWasFound = true
        print("transponderReceived")
        print("epc :\(epc), crc: \(crc), pc: \(pc), rssi: \(rssi) , fastId: \(fastId), moreAvailable:\(moreAvailable)")
        if (rssi != nil) {
            print("rssi isn't nil")
            self.addTagWithEPC(epc: epc ?? "", crc: 0, pc: 0, rssi: rssi, fastId: NSData())
        }
    }
    
    func addTagWithEPC(epc : String, crc : NSNumber, pc : NSNumber, rssi : NSNumber, fastId : NSData){
        if epcTag == epc {
            let newTag:Tag = Tag()
            newTag.epc = epc as String
            newTag.rssi = rssi
            let value:Int = Int(newTag.rssi)
            
            if ((Float(value * -1)) > self.minRSSI) {
                self.minRSSI = (Float(value * -1))
            }
            
            if ((Float(value * -1)) < self.maxRSSI) {
            self.maxRSSI = (Float(value * -1)) + 3.0
            }
            
            let range : Float = (((Float(value * -1) - (minRSSI)) / (maxRSSI - minRSSI)) * maximum)
            
            let rangeInt: Int = Int(range)
            print(rangeInt)
            self.resultLabel.text = "RSSI=\(value)"
            self.viewCustome(value: rangeInt)
        }
        Timer.scheduledTimer(timeInterval: 0.11, target: self, selector: #selector(TagFinderViewController.tagWasFoundOff), userInfo: nil, repeats: false)
    }
    
    func tagWasFoundOff()
    {
        self.tagWasFound = false
    }
    
    func scan() {
        if(commander?.isConnected == true) {
             self.commander?.execute(self.inventoryResponder)
        }
    }
    
    func scanB(_ sender: UIButton)
    {
        if self.scanningButton {
            self.scanButton.setTitle("Scan", for: .normal)
            self.scanTimer?.invalidate()
            self.scanningButton = false
        }else {
            self.scanButton.setTitle("Scanning", for: .normal)
            self.scanTimer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(TagFinderViewController.scan), userInfo: nil, repeats: true)
            self.scanningButton = true
        }
    }
    
    func switchStateReceived(_ state: TSL_SwitchState) {
    }
    
    func pollTrigger() {
        if self.commander?.isConnected == true {
            self.ssCommand = TSLSwitchStateCommand()
            self.ssCommand = TSLSwitchStateCommand.synchronousCommand()
            self.commander1?.execute(self.ssCommand)
            self.updatePolledTriggerStatusView(state: (ssCommand?.state)!)
        }
    }
    
    func updatePolledTriggerStatusView(state: TSL_SwitchState){
        
        switch(state)
        {
            case TSL_SwitchState_Off:
                if(!self.tagWasFound){
                    self.viewCustome(value: 0)
                }
                break;
            case TSL_SwitchState_Single:
                break;
            default:
                break;
        }
    }
    
    //pragma - mark
    func viewCustomeDefault(){
        self.sliderView.value = 19.5
        if (UIScreen.main.traitCollection.userInterfaceIdiom == .pad) {
            self.oneView.layer.cornerRadius = 22.5
            self.oneView.alpha = 0.1
            self.twoView.layer.cornerRadius = 22.5
            self.twoView.alpha = 0.1
            self.threeView.layer.cornerRadius = 22.5
            self.threeView.alpha = 0.1
            self.fourView.layer.cornerRadius = 22.5
            self.fourView.alpha = 0.1
            self.fiveView.layer.cornerRadius = 22.5
            self.fiveView.alpha = 0.1
            self.sixView.layer.cornerRadius = 22.5
            self.sixView.alpha = 0.1
            self.sevenView.layer.cornerRadius = 22.5
            self.sevenView.alpha = 0.1
            self.eightView.layer.cornerRadius = 22.5
            self.eightView.alpha = 0.1
            self.nineView.layer.cornerRadius = 22.5
            self.nineView.alpha = 0.1
            self.tenView.layer.cornerRadius = 22.5
            self.tenView.alpha = 0.1
        }
        else{
            self.oneView.layer.cornerRadius = 9
            self.oneView.alpha = 0.1
            self.twoView.layer.cornerRadius = 9
            self.twoView.alpha = 0.1
            self.threeView.layer.cornerRadius = 9
            self.threeView.alpha = 0.1
            self.fourView.layer.cornerRadius = 9
            self.fourView.alpha = 0.1
            self.fiveView.layer.cornerRadius = 9
            self.fiveView.alpha = 0.1
            self.sixView.layer.cornerRadius = 9
            self.sixView.alpha = 0.1
            self.sevenView.layer.cornerRadius = 9
            self.sevenView.alpha = 0.1
            self.eightView.layer.cornerRadius = 9
            self.eightView.alpha = 0.1
            self.nineView.layer.cornerRadius = 9
            self.nineView.alpha = 0.1
            self.tenView.layer.cornerRadius = 9
            self.tenView.alpha = 0.1
            
        }
        
    }
    
    func animationAppear (view : UIView)
    {
        UIView.animate(withDuration: 0.1, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
            view.alpha = 1;
        }) { (Bool) -> Void in
        }
    }
    
    func animationDisappear(view : UIView){
        UIView.animate(withDuration: 0.1, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
            view.alpha = 0.1;
        }) { (Bool) -> Void in
        }
    }
    
    var timeBeforeToBeep = NSDate().timeIntervalSince1970
    func viewCustome(value : Int){
        let timeToBeep = NSDate().timeIntervalSince1970
        if timeToBeep - self.timeBeforeToBeep <= 0.11 && value > 0{
            return
        }
        self.timeBeforeToBeep = timeToBeep
        self.firstBeep?.stop()
        self.secondBeep?.stop()
        self.thirdBeep?.stop()
        self.fourthBeep?.stop()
        if( value <= 1){
            self.animationDisappear(view: self.oneView)
            self.animationDisappear(view: self.twoView)
            self.animationDisappear(view: self.threeView)
            self.animationDisappear(view: self.fourView)
            self.animationDisappear(view: self.fiveView)
            self.animationDisappear(view: self.sixView)
            self.animationDisappear(view: self.sevenView)
            self.animationDisappear(view: self.eightView)
            self.animationDisappear(view: self.nineView)
            self.animationDisappear(view: self.tenView)
        }else if(value >= 2 && value <= 10){
            self.animationAppear(view: self.oneView)
            self.animationDisappear(view: self.twoView)
            self.animationDisappear(view: self.threeView)
            self.animationDisappear(view: self.fourView)
            self.animationDisappear(view: self.fiveView)
            self.animationDisappear(view: self.sixView)
            self.animationDisappear(view: self.sevenView)
            self.animationDisappear(view: self.eightView)
            self.animationDisappear(view: self.nineView)
            self.animationDisappear(view: self.tenView)
            if(self.soundIsOn){
                self.firstBeep?.play()
            }
        }
        else if(value >= 11 && value <= 20){
            self.animationAppear(view: self.oneView)
            self.animationAppear(view: self.twoView)
            self.animationDisappear(view: self.threeView)
            self.animationDisappear(view: self.fourView)
            self.animationDisappear(view: self.fiveView)
            self.animationDisappear(view: self.sixView)
            self.animationDisappear(view: self.sevenView)
            self.animationDisappear(view: self.eightView)
            self.animationDisappear(view: self.nineView)
            self.animationDisappear(view: self.tenView)
        }else if(value >= 21 && value <= 30){
            if(self.soundIsOn){
                self.firstBeep?.play()
            }
            self.animationAppear(view: self.oneView)
            self.animationAppear(view: self.twoView)
            self.animationAppear(view: self.threeView)
            self.animationDisappear(view: self.fourView)
            self.animationDisappear(view: self.fiveView)
            self.animationDisappear(view: self.sixView)
            self.animationDisappear(view: self.sevenView)
            self.animationDisappear(view: self.eightView)
            self.animationDisappear(view: self.nineView)
            self.animationDisappear(view: self.tenView)
        }else if(value >= 31 && value <= 40){
            if(self.soundIsOn){
                self.secondBeep?.play()
            }
            self.animationAppear(view: self.oneView)
            self.animationAppear(view: self.twoView)
            self.animationAppear(view: self.threeView)
            self.animationAppear(view: self.fourView)
            self.animationDisappear(view: self.fiveView)
            self.animationDisappear(view: self.sixView)
            self.animationDisappear(view: self.sevenView)
            self.animationDisappear(view: self.eightView)
            self.animationDisappear(view: self.nineView)
            self.animationDisappear(view: self.tenView)
        } else if(value >= 41 && value <= 50) {
            if(self.soundIsOn){
                self.secondBeep?.play()
            }
            self.animationAppear(view: self.oneView)
            self.animationAppear(view: self.twoView)
            self.animationAppear(view: self.threeView)
            self.animationAppear(view: self.fourView)
            self.animationAppear(view: self.fiveView)
            self.animationDisappear(view: self.sixView)
            self.animationDisappear(view: self.sevenView)
            self.animationDisappear(view: self.eightView)
            self.animationDisappear(view: self.nineView)
            self.animationDisappear(view: self.tenView)
        } else if(value >= 51 && value <= 60) {
            if(self.soundIsOn){
                self.secondBeep?.play()
            }
            self.animationAppear(view: self.oneView)
            self.animationAppear(view: self.twoView)
            self.animationAppear(view: self.threeView)
            self.animationAppear(view: self.fourView)
            self.animationAppear(view: self.fiveView)
            self.animationAppear(view: self.sixView)
            self.animationDisappear(view: self.sevenView)
            self.animationDisappear(view: self.eightView)
            self.animationDisappear(view: self.nineView)
            self.animationDisappear(view: self.tenView)
        }else if(value >= 61 && value <= 70) {
            if(self.soundIsOn) {
                self.thirdBeep?.play()
            }
            self.animationAppear(view: self.oneView)
            self.animationAppear(view: self.twoView)
            self.animationAppear(view: self.threeView)
            self.animationAppear(view: self.fourView)
            self.animationAppear(view: self.fiveView)
            self.animationAppear(view: self.sixView)
            self.animationAppear(view: self.sevenView)
            self.animationDisappear(view: self.eightView)
            self.animationDisappear(view: self.nineView)
            self.animationDisappear(view: self.tenView)
        }else if(value >= 71 && value <= 80){
            if(self.soundIsOn){
                self.thirdBeep?.play()
            }
            self.animationAppear(view: self.oneView)
            self.animationAppear(view: self.twoView)
            self.animationAppear(view: self.threeView)
            self.animationAppear(view: self.fourView)
            self.animationAppear(view: self.fiveView)
            self.animationAppear(view: self.sixView)
            self.animationAppear(view: self.sevenView)
            self.animationAppear(view: self.eightView)
            self.animationDisappear(view: self.nineView)
            self.animationDisappear(view: self.tenView)
        }else if(value >= 81 && value <= 90) {
            if(self.soundIsOn){
                self.fourthBeep?.play()
            }
            self.animationAppear(view: self.oneView)
            self.animationAppear(view: self.twoView)
            self.animationAppear(view: self.threeView)
            self.animationAppear(view: self.fourView)
            self.animationAppear(view: self.fiveView)
            self.animationAppear(view: self.sixView)
            self.animationAppear(view: self.sevenView)
            self.animationAppear(view: self.eightView)
            self.animationAppear(view: self.nineView)
            self.animationDisappear(view: self.tenView)
        }else if(value >= 91) {
            if(self.soundIsOn) {
                self.fourthBeep?.play()
            }
            self.animationAppear(view: self.oneView)
            self.animationAppear(view: self.twoView)
            self.animationAppear(view: self.threeView)
            self.animationAppear(view: self.fourView)
            self.animationAppear(view: self.fiveView)
            self.animationAppear(view: self.sixView)
            self.animationAppear(view: self.sevenView)
            self.animationAppear(view: self.eightView)
            self.animationAppear(view: self.nineView)
            self.animationAppear(view: self.tenView)
        }
    }
    
    @IBAction func outPutPowerEditingDidEnd(_ sender: UISlider) {
        print("outputPowerEditingDidEnd")
        let value:Int = Int(self.sliderView.value)
        self.sliderView.setValue(Float(value), animated: true)
        //self.setReaderOutputPower()
        if(commander?.isConnected == true){
            let value : Int32 = Int32(self.sliderView.value)
            var command : TSLInventoryCommand?
            command = TSLInventoryCommand.synchronousCommand()
            command?.takeNoAction = TSL_TriState_YES
            command?.outputPower = value
            commander?.execute(command)
        }
    }
    
    @IBAction func outPutPowerChanged(_ sender: UISlider) {
        let value:Int = Int(self.sliderView.value)
        let minRSSI: Float = 10
        let maxRSSI: Float = 29
        let maximum : Float = 100
        let range : Float = ((Float(value) - minRSSI) / (maxRSSI -  minRSSI) * maximum)
        let rangeInt: Int = Int(range)
        
        self.outputSliderLabel.text = String(format: "%i%@",rangeInt,"%")
    }
    
    @IBAction func goDetailView(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: ViewController_Name.InventoryDetailViewController) as! InventoryDetailViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
