//
//  SelectOptionTableViewCell.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/21/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

class SelectOptionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
