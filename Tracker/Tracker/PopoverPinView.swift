//
//  PopoverPinView.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 6/3/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

class PopoverPinView: UIView {
    
    let nibName = Xib_Name.popoverPinView
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var tagLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    // Values
    var checkedBool = false
    
    // MARK: View Methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.xibSetup()
    }
    
    func xibSetup() {
        self.view = loadViewFromNib()
        self.view.frame = bounds
        self.view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        self.addSubview(self.view)
        self.view.layer.cornerRadius = 5.0
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func configureView(serial: String, name: String) {
        self.tagLabel.text = serial
        self.nameLabel.text = name
    }
    


}
