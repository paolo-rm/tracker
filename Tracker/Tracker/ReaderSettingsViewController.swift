//
//  ReaderSettingsViewController.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/21/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit
import MRProgress

class ReaderSettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, TSLSelectReaderProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var accessoryList = [EAAccessory]()
    var singlePercentageLabel = UILabel()
    var multiplePercentageLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.accessoryList = EAAccessoryManager.shared().connectedAccessories
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: Class methods
    func configureView() {
        UtilView.getImageForBackground(name: Constant_Key.invBg, view: self.view)
        self.navigationController?.presentTransparentNavigationBar()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorInset = UIEdgeInsets.zero
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        self.tableView.separatorColor = UIColor.clear
        self.tableView.register(UINib(nibName: Constants.idReaderTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.idReaderTableViewCell)
        self.tableView.register(UINib(nibName: Constants.idSwitchTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.idSwitchTableViewCell)
        self.tableView.register(UINib(nibName: Constants.idProgressTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.idProgressTableViewCell)
        self.tableView.register(UINib(nibName: Constants.idSelectOptionTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.idSelectOptionTableViewCell)

    }
    
    // MARK: Table View methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.idReaderTableViewCell, for: indexPath) as! ReaderTableViewCell
            cell.readerImage.image = UIImage(named: Constant_Key.iconNoReader)
            cell.readerNameLabel.text = "No Reader (Select)"
            cell.readerBatteryStatusLabel.text = "Battery Level"
            if let commander = self.appDelegate.commander {
                 if commander.isConnected {
                    cell.readerImage.image = UIImage(named: Constant_Key.iconReader)
                    cell.readerNameLabel.text = commander.connectedAccessory.serialNumber
                    cell.readerBatteryStatusLabel.text = TSLReaderConfiguration.getBatteryValue()
                }
            }
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.idSwitchTableViewCell, for: indexPath) as! SwitchTableViewCell
            cell.titleLabel.text = "Sound"
            cell.viewSwitch.isOn = TSLLocalConfig.getReaderSoundValue()
            cell.viewSwitch.addTarget(self, action: #selector(self.soundValue(sender:)), for: UIControlEvents.valueChanged)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.idSwitchTableViewCell, for: indexPath) as! SwitchTableViewCell
            cell.titleLabel.text = "Vibrate"
            cell.viewSwitch.isOn = TSLLocalConfig.getReaderVibrateValue()
            cell.viewSwitch.addTarget(self, action: #selector(self.vibrateValue(sender:)), for: UIControlEvents.valueChanged)
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.idProgressTableViewCell, for: indexPath) as! ProgressTableViewCell
            cell.titleLabel.text = "Single"
            cell.percentageLabel.text = "\(TSLLocalConfig.getSimplePowerReader())%"
            cell.slider.value = Float(TSLLocalConfig.getSimplePowerReader())
            cell.slider.addTarget(self, action: #selector(self.singleSliderValue(sender:)), for: UIControlEvents.valueChanged)
            self.singlePercentageLabel = cell.percentageLabel
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.idProgressTableViewCell, for: indexPath) as! ProgressTableViewCell
            cell.titleLabel.text = "Multiple"
            cell.percentageLabel.text = "\(TSLLocalConfig.getMultiplePowerReader())%"
            cell.slider.value = Float(TSLLocalConfig.getMultiplePowerReader())
            cell.slider.addTarget(self, action: #selector(self.multipleSliderValue(sender:)), for: UIControlEvents.valueChanged)
            self.multiplePercentageLabel = cell.percentageLabel
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.idSelectOptionTableViewCell, for: indexPath) as! SelectOptionTableViewCell
            cell.titleLabel.text = "Disconnect / Turn off reader"
            return cell
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.idSelectOptionTableViewCell, for: indexPath) as! SelectOptionTableViewCell
            cell.titleLabel.text = "Reset to factory Defaults"
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: ViewController_Name.TSLSelectReaderViewController) as! TSLSelectReaderViewController
            vc.modalTransitionStyle = .crossDissolve
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.row == 5 {
            DispatchQueue.global(qos: .userInitiated).async {
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Reader", message: "Do you want to disconnect RFID Reader?", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.cancel, handler: { (action) in
                        TSLReaderConfiguration.disconnect()
                        self.tableView.reloadData()
                    }))
                    alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.destructive, handler: { (action) in
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        if indexPath.row == 6 {
            DispatchQueue.global(qos: .userInitiated).async {
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Reader", message: "Do you want to reset to factory defaults?", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.cancel, handler: { (action) in
                        TSLReaderConfiguration.resetTSLReader()
                        self.tableView.reloadData()
                    }))
                    alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.destructive, handler: { (action) in
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 70.0
        }
        return 65.0
    }
    
    // MARK: Reader methods
    func didSelectReader(forRow row: Int) {
        let progressView = MRProgressOverlayView()
        self.view.addSubview(progressView)
        progressView.show(true)
        DispatchQueue.global(qos: .userInitiated).async {
            // Bounce back to the main thread to update the UI
            DispatchQueue.main.async {
                if self.accessoryList.count > 0 {
                    let accessory = self.accessoryList[row]
                    self.appDelegate.commander?.connect(accessory)
                    if let commander = self.appDelegate.commander {
                        if commander.isConnected {
                            TSLReaderConfiguration.setRFIDReaderValues(TSLCommander: commander)
                        }
                    }
                }
                self.tableView.reloadData()
                progressView.dismiss(true)
            }
        }
    }
    
    func soundValue(sender: UISwitch) {
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                TSLLocalConfig.setReaderSoundValue(bool: sender.isOn)
                TSLReaderConfiguration.setSoundReader()
            }
        }
    }
    
    func vibrateValue(sender: UISwitch) {
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                TSLLocalConfig.setReaderVibrateValue(bool: sender.isOn)
                TSLReaderConfiguration.setVibrateReader()
            }
        }
    }
    
    func singleSliderValue(sender: UISlider) {
        TSLLocalConfig.setSimplePowerReader(value: Int(sender.value))
        self.singlePercentageLabel.text = "\(Int(sender.value))%"
    }

    func multipleSliderValue(sender: UISlider) {
        TSLLocalConfig.setMultiplePowerReader(value: Int(sender.value))
        self.multiplePercentageLabel.text = "\(Int(sender.value))%"
    }
}
