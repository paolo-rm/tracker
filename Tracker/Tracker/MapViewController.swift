//
//  MapViewController.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/31/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit
import MapKit
import Popover

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
         
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "annotationView")
        if annotationView == nil {
            annotationView = AnnotationView(annotation: annotation, reuseIdentifier: "annotationView")
            annotationView?.image = UIImage(named: Constant_Key.mapPinBlue)
            annotationView?.canShowCallout = false
        }
        else {
            annotationView?.annotation = annotation
        }
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKCircleRenderer(overlay: overlay)
        renderer.fillColor = UIColor.black.withAlphaComponent(0.5)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 2
        return renderer
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if view.annotation is MKUserLocation {
            return
        }
        let placeAnnotation = view.annotation as! Place
        let calloutView = PopoverPinView(frame: CGRect(x: 0.0, y: 0.0, width: 300.0, height: 60.0))
        calloutView.configureView(serial: placeAnnotation.title ?? "", name: placeAnnotation.subtitle ?? "")
        calloutView.center = CGPoint(x: view.bounds.size.width / 2, y: -calloutView.bounds.size.height*0.52)
        view.addSubview(calloutView)
        mapView.setCenter((view.annotation?.coordinate)!, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if view.isKind(of: AnnotationView.self)
        {
            for subview in view.subviews
            {
                subview.removeFromSuperview()
            }
        }
    }
    
}

class MapViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var reportMap: MKMapView!
    @IBOutlet weak var mapSegmented: UISegmentedControl!
    
    let locationManager = CLLocationManager()
    let places = Place.getPlaces()
    let tableView = UITableView()
//    let reportArray = [String]()
    let reportArray = ["report 1", "report 2", "report 3", "report 4"]
    let filterPopover = Popover()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.configureView()
        self.requestLocationAccess()
        self.addAnnotations()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Class methods
    func configureView() {
        UtilView.getImageForBackground(name: Constant_Key.invBg, view: self.view)
        UtilView.configureBackButton(vc: self, selector: #selector(self.goBack))
        self.navigationController?.presentTransparentNavigationBar()
        self.mapSegmented.layer.cornerRadius = 5.0
        self.reportMap.showsUserLocation = true
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.isScrollEnabled = true
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func requestLocationAccess() {
        let status = CLLocationManager.authorizationStatus()
        
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            return
            
        case .denied, .restricted:
            print("location access denied")
            
        default:
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func addAnnotations() {
        self.reportMap.delegate = self
        self.reportMap.addAnnotations(places)
        
        let overlays = places.map { MKCircle(center: $0.coordinate, radius: 100) }
        self.reportMap.addOverlays(overlays)
    }
    
    @IBAction func reportFilter(_ sender: UIBarButtonItem) {
        if self.reportArray.count > 0 {
            let nroRows : CGFloat
            if self.reportArray.count >= 10{
                nroRows = 44 * 10
            }
            else{
                nroRows = 44 * CGFloat(self.reportArray.count)
            }
            self.tableView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width - 44, height: nroRows)
            self.filterPopover.show(self.tableView, point: CGPoint(x: self.view.frame.width - 35.0, y: 60.0))
        }
    }
    
    @IBAction func changeKindOfMap(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            self.reportMap.mapType = MKMapType.standard
            break
        case 1:
            self.reportMap.mapType = MKMapType.satellite
            break
        case 2:
            self.reportMap.mapType = MKMapType.hybrid
            break
        default:
            self.reportMap.mapType = MKMapType.standard
            break
        }
        
    }
    
    // MARK: Table View methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.reportArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = self.reportArray[indexPath.row]
        return cell
    }
}
