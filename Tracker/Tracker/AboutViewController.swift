//
//  AboutViewController.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/21/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Configure View
    func configureView() {
        UtilView.getImageForBackground(name: Constant_Key.splashBg, view: self.view)
        self.navigationController?.presentTransparentNavigationBar()
    }

}
