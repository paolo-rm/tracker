//
//  InformationTableViewCell.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/21/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

class InformationTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var lastSyncLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        UtilView.getImageForBackground(name: Constant_Key.invCellViewBg, view: self)
        self.nameLabel.backgroundColor = UIColor.clear
        self.lastSyncLabel.backgroundColor = UIColor.clear
        self.tintColor = UIColor.white
        self.accessoryView?.tintColor = UIColor.white
        self.preservesSuperviewLayoutMargins = false
        self.separatorInset = UIEdgeInsets.zero
        self.layoutMargins = UIEdgeInsets.zero
    }
    
}
