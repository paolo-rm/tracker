//
//  ReaderTableViewCell.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/21/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

class ReaderTableViewCell: UITableViewCell {

    @IBOutlet weak var readerImage: UIImageView!
    @IBOutlet weak var readerNameLabel: UILabel!
    @IBOutlet weak var readerBatteryStatusLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        self.backgroundColor = UIColor.clear
        self.readerImage.backgroundColor = UIColor.clear
        self.readerNameLabel.backgroundColor = UIColor.clear
        self.readerBatteryStatusLabel.backgroundColor = UIColor.clear
        self.tintColor = UIColor.white
        self.accessoryView?.tintColor = UIColor.white
        self.preservesSuperviewLayoutMargins = false
        self.separatorInset = UIEdgeInsets.zero
        self.layoutMargins = UIEdgeInsets.zero
        
    }
    
}
