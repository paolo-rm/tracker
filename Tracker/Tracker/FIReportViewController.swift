//
//  FIReportViewController.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/29/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

class FIReportViewController: UIViewController {

    var inventoryView: InventoryView!
    class var selectFIReport: String { return "selectFIReport" }
    class var selectFIScanMode: String { return "selectFIScanMode" }
    class var goNext: String { return "goNext" }

    var reportString = InventoryView.None
    var scanModeString = InventoryView.None

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.configureView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.selectFIReport(notification:)),
                                               name: NSNotification.Name(rawValue: FIReportViewController.selectFIReport),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.selectFIScanMode(notification:)),
                                               name: NSNotification.Name(rawValue: FIReportViewController.selectFIScanMode),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.goNext(notification:)),
                                               name: NSNotification.Name(rawValue: FIReportViewController.goNext),
                                               object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.inventoryView.updateModule(moduleValue: Module.FindIt, report: self.reportString, scanMode: self.scanModeString)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: FIReportViewController.selectFIReport), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: FIReportViewController.selectFIScanMode), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: FIReportViewController.goNext), object: nil)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Class methods
    func configureView() {
        UtilView.getImageForBackground(name: Constant_Key.invBg, view: self.view)
        self.navigationController?.presentTransparentNavigationBar()
        
        self.inventoryView = InventoryView(frame: UtilView.getContainerFrame(viewController: self, notificationHeight: 60.0))
        self.inventoryView.updateModule(moduleValue: Module.FindIt, report: InventoryView.None, scanMode: InventoryView.None)
        self.view.addSubview(self.inventoryView)
    }
    
    let reportArray = ["Report 1", "Report 2", "Report 3", "Report 4", "Report 5"]
    let scanModeArray = ["Single", "Multiple"]
    
    func selectFIReport(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: ViewController_Name.FISelectReportViewController) as! FISelectReportViewController
        vc.module = Module.FindIt
        vc.section = Section.Report
        vc.arrayString = self.reportArray
        vc.checkControl = self.reportString
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func selectFIScanMode(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: ViewController_Name.FISelectReportViewController) as! FISelectReportViewController
        vc.module = Module.FindIt
        vc.section = Section.ScanMode
        vc.arrayString = self.scanModeArray
        vc.checkControl = self.scanModeString
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goNext(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: ViewController_Name.FIReportListViewController) as! FIReportListViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
