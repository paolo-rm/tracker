//
//  InventoryViewController.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 4/30/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

class InventoryViewController: UIViewController {

    var inventoryView: InventoryView!
    class var selectInventoryReport: String { return "selectInventoryReport" }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.selectInventoryReport(notification:)),
                                               name: NSNotification.Name(rawValue: InventoryViewController.selectInventoryReport),
                                               object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: InventoryViewController.selectInventoryReport), object: nil)
    }
    
    // MARK: Class methods
    func configureView() {
        UtilView.getImageForBackground(name: Constant_Key.invBg, view: self.view)
        self.navigationController?.presentTransparentNavigationBar()
        
        self.inventoryView = InventoryView(frame: UtilView.getContainerFrame(viewController: self, notificationHeight: 60.0))
        self.view.addSubview(self.inventoryView)
    }
    
    func selectInventoryReport(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: ViewController_Name.inventoryReportViewController) as! InventoryReportViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
