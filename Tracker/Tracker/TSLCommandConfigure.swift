//
//  TSLCommandConfigure.swift
//  Tracker
//
//  Created by Paolo Ramos Mendez on 5/27/17.
//  Copyright © 2017 Paolo Ramos. All rights reserved.
//

import UIKit

enum TSLOPtion : Int {
    case Inventory
    case FindIt
}

@objc protocol TSLCommandConfigureDelegate: class {
    @objc optional func TSLTransponderReceived(epc: String!, crc: NSNumber!, pc: NSNumber!, rssi: NSNumber!, fastId: Data!, moreAvailable: Bool)
    @objc optional func TSLFindItTransponderReceived(epc: String!, crc: NSNumber!, pc: NSNumber!, rssi: NSNumber!, fastId: Data!, moreAvailable: Bool)
    @objc optional func TSLBarcodeReceived(data: String)
}

class TSLCommandConfigure: NSObject, TSLInventoryCommandTransponderReceivedDelegate, TSLBarcodeCommandBarcodeReceivedDelegate {
    
    var scanMultipleValue = Bool()
    var option = TSLOPtion.Inventory
    weak var delegate: TSLCommandConfigureDelegate?
    
    override init() {
        super.init()
        self.scanMultipleValue = Bool()
    }
    
    init(isMultipleScan: Bool) {
        super.init()
        self.scanMultipleValue = isMultipleScan
        self.clearReaderResponder(isMultipleScan: self.scanMultipleValue)
    }
    
    init(epcTag: String) {
        super.init()
        self.clearResponderToFindTag(epcTag: epcTag)
    }
    
    func clearReaderResponder(isMultipleScan: Bool) {
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        self.option = TSLOPtion.Inventory
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                if let commander = appDelegate.commander {
                    if commander.isConnected {
                        commander.clearResponders()
                        
                        let loggerResponder:TSLLoggerResponder = TSLLoggerResponder()
                        commander.add(loggerResponder)
                        commander.addSynchronousResponder()
                        
                        let inventoryResponder = TSLInventoryCommand()
                        inventoryResponder.transponderReceivedDelegate = self
                        inventoryResponder.captureNonLibraryResponses = true
                        inventoryResponder.includeTransponderRSSI = TSL_TriState_YES
                        commander.add(inventoryResponder)
                        
                        let value : Int32
                        if isMultipleScan {
                            TSLReaderConfiguration.setRFIDMultipleScanValues()
                            value = Int32(TSLReaderConfiguration.getTslValue(valueIn: Float(TSLLocalConfig.getMultiplePowerReader())))
                        }
                        else {
                            TSLReaderConfiguration.setRFIDSingleScanValues()
                            value = Int32(TSLReaderConfiguration.getTslValue(valueIn: Float(TSLLocalConfig.getSimplePowerReader())))
                        }
                        
                        let barcodeResponder = TSLBarcodeCommand()
                        barcodeResponder.barcodeReceivedDelegate = self
                        barcodeResponder.captureNonLibraryResponses = true
                        commander.add(barcodeResponder)
                        
                        if let command = TSLInventoryCommand.synchronousCommand() {
                            command.takeNoAction = TSL_TriState_YES
                            command.outputPower = value
                            commander.execute(command)
                        }
                        
                        if let command = TSLSwitchActionCommand.synchronousCommand() {
                            command.asynchronousReportingEnabled = TSL_TriState_NO
                            command.resetParameters = TSL_TriState_YES
                            commander.execute(command)
                        }
                    }
                }
            }
        }
    }
    
    func clearResponderToFindTag(epcTag: String) {
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        self.option = TSLOPtion.FindIt
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                if let commander = appDelegate.commander {
                    if commander.isConnected {
                        commander.clearResponders()
                        commander.addSynchronousResponder()
                        let inventoryResponder = TSLInventoryCommand()
                        inventoryResponder.transponderReceivedDelegate = self
                        inventoryResponder.captureNonLibraryResponses = true
                        inventoryResponder.includeTransponderRSSI = TSL_TriState_YES
                        commander.add(inventoryResponder)
                        TSLReaderConfiguration.setRFIDFindItValues(epcTag: epcTag)
                        if let saCommand = TSLSwitchActionCommand.synchronousCommand() {
                            saCommand.asynchronousReportingEnabled = TSL_TriState_NO
                            saCommand.resetParameters = TSL_TriState_YES
                            commander.execute(saCommand)
                        }
                    }
                }
            }
        }
    }
    
    var counterT : Int = 0
    func transponderReceived(_ epc: String!, crc: NSNumber!, pc: NSNumber!, rssi: NSNumber!, fastId: Data!, moreAvailable: Bool) {
        print("transponderReceived ***: \(epc ?? "")")
        switch self.option {
        case TSLOPtion.Inventory:
            DispatchQueue.global(qos: .userInitiated).async {
                DispatchQueue.main.async {
                    if moreAvailable {
                        self.counterT = self.counterT + 1
                    }
                    if !self.scanMultipleValue {
                        if moreAvailable {
                            print("Too many tags")
                        }
                        else {
                            if self.counterT < 1 {
                                self.delegate?.TSLTransponderReceived!(epc: epc, crc: crc, pc: pc, rssi: rssi, fastId: fastId, moreAvailable: moreAvailable)
                            }
                            self.counterT = 0
                        }
                    }
                    else {
                        self.delegate?.TSLTransponderReceived!(epc: epc, crc: crc, pc: pc, rssi: rssi, fastId: fastId, moreAvailable: moreAvailable)
                    }
                }
            }
            break
        case TSLOPtion.FindIt:
            DispatchQueue.global(qos: .userInitiated).async {
                DispatchQueue.main.async {
                    if rssi != nil {
                        self.delegate?.TSLFindItTransponderReceived!(epc: epc, crc: crc, pc: pc, rssi: rssi, fastId: fastId, moreAvailable: moreAvailable)
                    }
                }
            }
            break
        }
    }
    
    func barcodeReceived(_ data: String) {
        print("barcodeReceived ***: \(data)")
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.delegate?.TSLBarcodeReceived!(data: data)
            }
        }
    }


}
